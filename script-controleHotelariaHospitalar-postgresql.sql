DELETE FROM recurso;

INSERT INTO recurso(nome, quantidade)
VALUES ('Quarto individual', 14);

INSERT INTO recurso(nome, quantidade)
VALUES ('Quarto duplo', 8);

INSERT INTO recurso(nome, quantidade)
VALUES ('Quarto triplo', 10);

INSERT INTO recurso(nome, quantidade)
VALUES ('Lençol de cama', 91);

INSERT INTO recurso(nome, quantidade)
VALUES ('Toalha de banho', 63);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Adulto P', 25);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Adulto M', 36);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Adulto G', 37);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Adulto GG', 18);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Infantil P', 17);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Infantil M', 30);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Infantil G', 42);

INSERT INTO recurso(nome, quantidade)
VALUES ('Roupa Infantil GG', 26);