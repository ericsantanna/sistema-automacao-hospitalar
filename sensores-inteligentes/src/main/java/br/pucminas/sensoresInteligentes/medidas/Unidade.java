package br.pucminas.sensoresInteligentes.medidas;

/**
 * @author Eric Sant'Anna
 */
public class Unidade {
    public enum Pressao {MMHG}
    public enum FrequenciaCardiaca {BPM}
}
