package br.pucminas.sensoresInteligentes.batimentoCardiaco;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@XmlRootElement
@Data @NoArgsConstructor
public class BatimentoCardiaco implements Serializable {
    private static final long serialVersionUID = -9199948881744376171L;

    private int batimentosPorMinuto;

    public BatimentoCardiaco(int bpm) {
        batimentosPorMinuto = bpm;
    }
}
