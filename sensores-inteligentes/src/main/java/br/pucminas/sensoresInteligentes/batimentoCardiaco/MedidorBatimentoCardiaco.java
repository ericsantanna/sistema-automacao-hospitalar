package br.pucminas.sensoresInteligentes.batimentoCardiaco;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface MedidorBatimentoCardiaco {
    EquipamentoBatimentoCardiaco get(long id);
    List<EquipamentoBatimentoCardiaco> list();
    BatimentoCardiaco aferirBatimentosCardiaco(long id);
}
