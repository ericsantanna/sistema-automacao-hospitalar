package br.pucminas.sensoresInteligentes.batimentoCardiaco;

import br.pucminas.sensoresInteligentes.sensores.Sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Eric Sant'Anna
 */
@Entity
@DiscriminatorValue("BATIMENTO_CARDIACO")
public class EquipamentoBatimentoCardiaco extends Sensor {
    private static final long serialVersionUID = -7624881779677324446L;
}
