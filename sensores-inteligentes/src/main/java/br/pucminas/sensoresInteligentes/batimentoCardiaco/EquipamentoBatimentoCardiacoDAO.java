package br.pucminas.sensoresInteligentes.batimentoCardiaco;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class EquipamentoBatimentoCardiacoDAO {
    @PersistenceContext
    private EntityManager em;

    public EquipamentoBatimentoCardiaco get(long id) {
        return em.find(EquipamentoBatimentoCardiaco.class, id);
    }

    public List<EquipamentoBatimentoCardiaco> list() {
        return em.createQuery("select e from EquipamentoBatimentoCardiaco e", EquipamentoBatimentoCardiaco.class).getResultList();
    }
}
