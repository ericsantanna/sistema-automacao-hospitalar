package br.pucminas.sensoresInteligentes.webservices;

import br.pucminas.sensoresInteligentes.accessLogger.AccessLoggerInterceptor;
import br.pucminas.sensoresInteligentes.medidas.Unidade;
import br.pucminas.sensoresInteligentes.pressaoArterial.EquipamentoPressaoArterial;
import br.pucminas.sensoresInteligentes.pressaoArterial.EquipamentoPressaoArterialDAO;
import br.pucminas.sensoresInteligentes.pressaoArterial.MedidorPressaoArterial;
import br.pucminas.sensoresInteligentes.pressaoArterial.PressaoArterial;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.sensoresInteligentes.pressaoArterial.MedidorPressaoArterial")
public class MedidorPressaoArterialWS implements MedidorPressaoArterial {

    @Inject private EquipamentoPressaoArterialDAO dao;

    @Override
    public EquipamentoPressaoArterial get(long id) {
        EquipamentoPressaoArterial equipamentoPressaoArterial = dao.get(id);
        return equipamentoPressaoArterial;
    }

    @Override
    public List<EquipamentoPressaoArterial> list() {
        return dao.list();
    }

    @Override
    public PressaoArterial aferirPressaoArterial(long id) {
        int sistolica = ThreadLocalRandom.current().nextInt(11, 15 + 1);
        int diastolica = sistolica - 5;
        return new PressaoArterial(sistolica, diastolica, Unidade.Pressao.MMHG);
    }
}
