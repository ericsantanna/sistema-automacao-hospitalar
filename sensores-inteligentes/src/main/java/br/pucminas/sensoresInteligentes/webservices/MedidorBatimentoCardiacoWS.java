package br.pucminas.sensoresInteligentes.webservices;

import br.pucminas.sensoresInteligentes.accessLogger.AccessLoggerInterceptor;
import br.pucminas.sensoresInteligentes.batimentoCardiaco.BatimentoCardiaco;
import br.pucminas.sensoresInteligentes.batimentoCardiaco.EquipamentoBatimentoCardiaco;
import br.pucminas.sensoresInteligentes.batimentoCardiaco.EquipamentoBatimentoCardiacoDAO;
import br.pucminas.sensoresInteligentes.batimentoCardiaco.MedidorBatimentoCardiaco;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.sensoresInteligentes.batimentoCardiaco.MedidorBatimentoCardiaco")
public class MedidorBatimentoCardiacoWS implements MedidorBatimentoCardiaco {

    @Inject private EquipamentoBatimentoCardiacoDAO dao;

    @Override
    public EquipamentoBatimentoCardiaco get(long id) {
        return dao.get(id);
    }

    @Override
    public List<EquipamentoBatimentoCardiaco> list() {
        return dao.list();
    }

    @Override
    public BatimentoCardiaco aferirBatimentosCardiaco(long id) {
        int bpm = ThreadLocalRandom.current().nextInt(60, 90 + 1);
        return new BatimentoCardiaco(bpm);
    }
}
