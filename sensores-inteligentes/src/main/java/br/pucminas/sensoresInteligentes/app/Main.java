package br.pucminas.sensoresInteligentes.app;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.config.logging.Level;
import org.wildfly.swarm.datasources.DatasourcesFraction;
import org.wildfly.swarm.logging.LoggingFraction;
import org.wildfly.swarm.undertow.WARArchive;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Eric Sant'Anna
 */
public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("properties file: " + System.getProperty("swarm.propertiesFile"));
        System.getProperties().load(Files.newInputStream(Paths.get(System.getProperty("swarm.propertiesFile"))));
        String logFile = System.getProperty("user.dir") + File.separator + "app.log";

        Swarm swarm = new Swarm(args);

        swarm.fraction(
                new DatasourcesFraction()
                .jdbcDriver("org.postgresql", (d) -> {
                    d.driverClassName("org.postgresql.Driver");
                    d.xaDatasourceClass("org.postgresql.xa.PGXADataSource");
                    d.driverModuleName("org.postgresql");
                })
                .dataSource("SensoresInteligentesDS,", (ds) -> {
                    ds.driverName("org.postgresql");
                    ds.jndiName("java:jboss/datasources/SensoresInteligentesDS");
                    ds.connectionUrl(System.getProperty("ds.connectionUrl"));
                    ds.userName(System.getProperty("ds.userName"));
                    ds.password(System.getProperty("ds.password"));
                    ds.checkValidConnectionSql("select 1");
                    ds.validateOnMatch(false);
                    ds.backgroundValidation(true);
                    ds.backgroundValidationMillis(10000L);
                })
        );

        swarm.fraction(
                new LoggingFraction()
                .fileHandler("FILE", f -> {
                    Map<String, String> fileProps = new HashMap<>();
                    fileProps.put("path", logFile);
                    f.file(fileProps);
                    f.level(Level.INFO);
                    f.append(false);
                    f.formatter("%d{HH:mm:ss.SSS} %-5p [%c] %s%e%n");

                })
                .consoleHandler("CONSOLE", c -> {
                    c.level(Level.INFO);
                    c.formatter("%d{HH:mm:ss.SSS} %-5p [%c] %s%e%n");
                })
                .rootLogger(Level.INFO, "FILE", "CONSOLE")
        );

        swarm.start();

        ClassLoader classLoader = Main.class.getClassLoader();

        WARArchive deployment = ShrinkWrap.create(WARArchive.class, "sensores-inteligentes.war");
        deployment.addPackages(true, "br.pucminas.sensoresInteligentes");
        deployment.addAsWebInfResource(classLoader.getResource("META-INF/persistence.xml"),"classes/META-INF/persistence.xml");
        deployment.addAllDependencies();

        swarm.deploy(deployment);
    }
}
