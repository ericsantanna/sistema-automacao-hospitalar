package br.pucminas.sensoresInteligentes.accessLogger;

import org.jboss.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.net.InetAddress;

/**
 * @author Eric Sant'Anna
 */
public class AccessLoggerInterceptor {

    private Logger log = Logger.getLogger(getClass());

    @AroundInvoke
    public Object accessLog(InvocationContext context) throws Exception {
        log.info("Host: " + InetAddress.getLocalHost().getHostName() + " - Action: " + context.getMethod().getName());
        return context.proceed();
    }
}
