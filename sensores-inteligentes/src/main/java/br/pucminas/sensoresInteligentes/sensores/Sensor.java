package br.pucminas.sensoresInteligentes.sensores;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */

@Entity
@Inheritance
@DiscriminatorColumn(name="tipo")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Data @NoArgsConstructor
public abstract class Sensor implements Serializable {

    public enum Status {OFFLINE, ONLINE, STANDBY}

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(insertable = false, updatable = false)
    private String tipo;

    private String serialNumber;
    private String marca;
    private String modelo;
    private String software;
    private String versao;

    @Enumerated(EnumType.STRING)
    private Status status = Status.OFFLINE;
}
