package br.pucminas.sensoresInteligentes.pressaoArterial;

import br.pucminas.sensoresInteligentes.sensores.Sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Eric Sant'Anna
 */
@Entity
@DiscriminatorValue("PRESSAO_ARTERIAL")
public class EquipamentoPressaoArterial extends Sensor {
    private static final long serialVersionUID = 5551084472512789735L;
}
