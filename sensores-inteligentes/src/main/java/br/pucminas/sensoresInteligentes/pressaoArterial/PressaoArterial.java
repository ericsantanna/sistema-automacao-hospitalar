package br.pucminas.sensoresInteligentes.pressaoArterial;

import br.pucminas.sensoresInteligentes.medidas.Unidade;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@XmlRootElement
@Data @NoArgsConstructor
public class PressaoArterial implements Serializable {
    private static final long serialVersionUID = 903931874760310713L;

    private int sistolica;
    private int diastolica;
    private Unidade.Pressao unidade;

    public PressaoArterial(int sistolica, int diastolica, Unidade.Pressao unidade) {
        this.sistolica = sistolica;
        this.diastolica = diastolica;
        this.unidade = unidade;
    }
}
