package br.pucminas.sensoresInteligentes.pressaoArterial;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class EquipamentoPressaoArterialDAO {
    @PersistenceContext
    private EntityManager em;

    public EquipamentoPressaoArterial get(long id) {
        return em.find(EquipamentoPressaoArterial.class, id);
    }

    public List<EquipamentoPressaoArterial> list() {
        return em.createQuery("select e from EquipamentoPressaoArterial e", EquipamentoPressaoArterial.class).getResultList();
    }
}
