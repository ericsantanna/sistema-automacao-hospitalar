package br.pucminas.sensoresInteligentes.pressaoArterial;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface MedidorPressaoArterial {
    EquipamentoPressaoArterial get(long id);
    List<EquipamentoPressaoArterial> list();
    PressaoArterial aferirPressaoArterial(long id);
}
