package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Basofilo extends Medida {
    private static final long serialVersionUID = 50332674281334861L;

    public Basofilo() {
        super("%");
    }
}
