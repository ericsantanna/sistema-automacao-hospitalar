package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma.neutrofilos;

import br.pucminas.laboratorioExames.exames.Medida;
import lombok.Data;

import javax.persistence.*;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
@Data
public class Neutrofilo extends Medida{
    private static final long serialVersionUID = -1234436684764247770L;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="mielocitos")))
    private Mielocito mielocito;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="metamielocitos")))
    private Metamielocito metamielocito;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="segmentados")))
    private Segmentado segmentado;

    public Neutrofilo() {
        super("%");
    }
}
