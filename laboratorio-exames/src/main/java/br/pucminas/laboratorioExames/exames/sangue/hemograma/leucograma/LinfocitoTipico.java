package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class LinfocitoTipico extends Medida {
    private static final long serialVersionUID = 7141589101702224419L;

    public LinfocitoTipico() {
        super("%");
    }
}
