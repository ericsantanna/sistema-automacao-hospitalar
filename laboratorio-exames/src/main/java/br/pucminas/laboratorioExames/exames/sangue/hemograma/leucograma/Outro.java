package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Outro extends Medida {
    private static final long serialVersionUID = 3164433710785621313L;

    public Outro() {
        super("%");
    }
}
