package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma.neutrofilos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Segmentado extends Medida {
    private static final long serialVersionUID = 8757646749715330227L;

    public Segmentado() {
        super("%");
    }
}
