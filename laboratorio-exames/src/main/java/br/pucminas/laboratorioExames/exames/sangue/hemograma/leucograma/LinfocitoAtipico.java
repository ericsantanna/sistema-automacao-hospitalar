package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class LinfocitoAtipico extends Medida {
    private static final long serialVersionUID = -8862588762645891344L;

    public LinfocitoAtipico() {
        super("%");
    }
}
