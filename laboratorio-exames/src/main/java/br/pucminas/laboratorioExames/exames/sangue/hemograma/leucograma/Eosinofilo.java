package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Eosinofilo extends Medida {
    private static final long serialVersionUID = -1283885362935534735L;

    public Eosinofilo() {
        super("%");
    }
}
