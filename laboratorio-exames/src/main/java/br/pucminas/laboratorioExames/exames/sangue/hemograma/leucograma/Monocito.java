package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Monocito extends Medida {
    private static final long serialVersionUID = -5996105960250805899L;

    public Monocito() {
        super("%");
    }
}
