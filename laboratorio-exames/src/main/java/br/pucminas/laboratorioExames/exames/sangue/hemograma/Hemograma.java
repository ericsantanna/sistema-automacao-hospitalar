package br.pucminas.laboratorioExames.exames.sangue.hemograma;

import br.pucminas.laboratorioExames.exames.Exame;
import br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.Eritograma;
import br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma.Leucograma;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Eric Sant'Anna
 */
@Entity
@Data @NoArgsConstructor
public class Hemograma extends Exame {
    private static final long serialVersionUID = -4004816058193154569L;

    @Embedded
    private Eritograma eritograma;

    @Embedded
    private Leucograma leucograma;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="plaquetas")))
    private Plaqueta plaqueta;
}
