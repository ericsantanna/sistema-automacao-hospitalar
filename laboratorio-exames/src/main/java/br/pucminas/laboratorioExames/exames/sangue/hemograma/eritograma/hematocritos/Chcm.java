package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.hematocritos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Chcm extends Medida {
    private static final long serialVersionUID = -2265750695406580609L;

    public Chcm() {
        super("g/dL");
    }
}
