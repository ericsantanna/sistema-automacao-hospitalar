package br.pucminas.laboratorioExames.exames;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@MappedSuperclass
@Data
public abstract class Medida implements Serializable {
    private static final long serialVersionUID = 1657190532885254412L;

    private double valor;

    @XmlElement
    @Transient
    private final String unidade;
}
