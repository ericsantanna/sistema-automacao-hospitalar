package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
@Data
public class Leucograma implements Serializable {
    private static final long serialVersionUID = -8527116120678424470L;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="leucocitos")))
    private Leucocito leucocito;
}
