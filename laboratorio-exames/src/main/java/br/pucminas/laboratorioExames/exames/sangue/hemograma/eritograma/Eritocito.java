package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Eritocito extends Medida {
    private static final long serialVersionUID = 7697645796253125629L;

    public Eritocito() {
        super("mm³");
    }
}
