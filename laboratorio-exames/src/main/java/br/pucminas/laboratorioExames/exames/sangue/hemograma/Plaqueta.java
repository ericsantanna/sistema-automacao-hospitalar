package br.pucminas.laboratorioExames.exames.sangue.hemograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Plaqueta extends Medida {
    private static final long serialVersionUID = 2903740643483357344L;

    public Plaqueta() {
        super("mm³");
    }
}
