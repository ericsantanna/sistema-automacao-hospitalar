package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma.neutrofilos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Mielocito extends Medida {
    private static final long serialVersionUID = 2501733570098739683L;

    public Mielocito() {
        super("%");
    }
}
