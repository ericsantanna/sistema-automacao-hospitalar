package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.hematocritos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Hcm extends Medida {
    private static final long serialVersionUID = 6107363496454695913L;

    public Hcm() {
        super("pg");
    }
}
