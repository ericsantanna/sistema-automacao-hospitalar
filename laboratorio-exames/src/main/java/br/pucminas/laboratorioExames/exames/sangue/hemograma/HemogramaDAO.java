package br.pucminas.laboratorioExames.exames.sangue.hemograma;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class HemogramaDAO {
    @PersistenceContext
    private EntityManager em;

    public Hemograma get(long id) {
        return em.find(Hemograma.class, id);
    }
    public List<Hemograma> list() {
        return em.createQuery("select h from Hemograma h", Hemograma.class).getResultList();
    }
}
