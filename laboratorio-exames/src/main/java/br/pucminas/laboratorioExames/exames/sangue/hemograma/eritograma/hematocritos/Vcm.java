package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.hematocritos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Vcm extends Medida {
    private static final long serialVersionUID = -2031847354638529104L;

    public Vcm() {
        super("fl");
    }
}
