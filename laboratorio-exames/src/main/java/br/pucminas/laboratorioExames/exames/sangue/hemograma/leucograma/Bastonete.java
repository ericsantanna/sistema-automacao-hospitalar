package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Bastonete extends Medida {
    private static final long serialVersionUID = -5632317401413741295L;

    public Bastonete() {
        super("%");
    }
}
