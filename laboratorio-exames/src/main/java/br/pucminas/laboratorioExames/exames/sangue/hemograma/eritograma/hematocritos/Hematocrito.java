package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.hematocritos;

import br.pucminas.laboratorioExames.exames.Medida;
import lombok.Data;

import javax.persistence.*;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
@Data
public class Hematocrito extends Medida {
    private static final long serialVersionUID = 4147201511536175147L;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="vcm")))
    private Vcm vcm;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="hcm")))
    private Hcm hcm;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="chcm")))
    private Chcm chcm;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="rdw")))
    private Rdw rdw;

    public Hematocrito() {
        super("%");
    }
}
