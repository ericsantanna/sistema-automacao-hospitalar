package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma.neutrofilos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Metamielocito extends Medida {
    private static final long serialVersionUID = 7754386357995111435L;

    public Metamielocito() {
        super("%");
    }
}
