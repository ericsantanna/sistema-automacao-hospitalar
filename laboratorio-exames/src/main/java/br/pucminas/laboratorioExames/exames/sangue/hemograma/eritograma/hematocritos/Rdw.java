package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.hematocritos;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Rdw extends Medida {
    private static final long serialVersionUID = -6308487379987455086L;

    public Rdw() {
        super("%");
    }
}
