package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma;

import br.pucminas.laboratorioExames.exames.Medida;

import javax.persistence.Embeddable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
public class Hemoglobina extends Medida {
    private static final long serialVersionUID = -2868178742848678924L;

    public Hemoglobina() {
        super("g/dL");
    }
}
