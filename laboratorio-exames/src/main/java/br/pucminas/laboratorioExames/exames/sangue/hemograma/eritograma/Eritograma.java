package br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma;

import br.pucminas.laboratorioExames.exames.sangue.hemograma.eritograma.hematocritos.Hematocrito;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
@Data
public class Eritograma implements Serializable {
    private static final long serialVersionUID = -6415388834189788135L;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="eritocitos")))
    private Eritocito eritocito;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="hemoglobinas")))
    private Hemoglobina hemoglobina;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="hematocritos")))
    private Hematocrito hematocrito;

}
