package br.pucminas.laboratorioExames.exames.sangue.hemograma;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface HemogramaWS {
    Hemograma get(long id);
    List<Hemograma> list();
}
