package br.pucminas.laboratorioExames.exames;

import br.pucminas.laboratorioExames.paciente.Paciente;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Eric Sant'Anna
 */
@MappedSuperclass
@XmlRootElement
@Data @NoArgsConstructor
public abstract class Exame implements Serializable {
    public enum Status {PENDENTE, PRONTO, NAO_REALIZADO}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date dataPedido;
    private Date dataEntrega;

    @Enumerated(EnumType.STRING)
    private Status status = Status.PENDENTE;

    @ManyToOne
    private Paciente paciente;
}
