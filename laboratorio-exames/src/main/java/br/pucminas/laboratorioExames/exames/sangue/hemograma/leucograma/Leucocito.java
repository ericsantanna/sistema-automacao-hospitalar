package br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma;

import br.pucminas.laboratorioExames.exames.Medida;
import br.pucminas.laboratorioExames.exames.sangue.hemograma.leucograma.neutrofilos.Neutrofilo;
import lombok.Data;

import javax.persistence.*;

/**
 * @author Eric Sant'Anna
 */
@Embeddable
@Data
public class Leucocito extends Medida {
    private static final long serialVersionUID = 1852366809114628573L;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="neutrofilos")))
    private Neutrofilo neutrofilo;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="linfocitosTipicos")))
    private LinfocitoTipico linfocitoTipico;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="monocitos")))
    private Monocito monocito;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="eosinofilos")))
    private Eosinofilo eosinofilo;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="basofilos")))
    private Basofilo basofilo;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="bastonetes")))
    private Bastonete bastonete;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="linfocitosAtipicos")))
    private LinfocitoAtipico linfocitoAtipico;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="valor", column=@Column(name="outros")))
    private Outro outro;

    public Leucocito() {
        super("mm³");
    }
}
