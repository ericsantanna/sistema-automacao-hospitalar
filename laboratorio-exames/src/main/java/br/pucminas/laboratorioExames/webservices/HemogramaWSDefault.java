package br.pucminas.laboratorioExames.webservices;

import br.pucminas.laboratorioExames.accessLogger.AccessLoggerInterceptor;
import br.pucminas.laboratorioExames.exames.sangue.hemograma.Hemograma;
import br.pucminas.laboratorioExames.exames.sangue.hemograma.HemogramaDAO;
import br.pucminas.laboratorioExames.exames.sangue.hemograma.HemogramaWS;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.laboratorioExames.exames.sangue.hemograma.HemogramaWS")
public class HemogramaWSDefault implements HemogramaWS {

    @Inject private HemogramaDAO dao;

    @Override
    public Hemograma get(long id) {
        return dao.get(id);
    }

    @Override
    public List<Hemograma> list() {
        return dao.list();
    }
}
