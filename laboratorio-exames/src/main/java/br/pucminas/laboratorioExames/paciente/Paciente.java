package br.pucminas.laboratorioExames.paciente;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@Entity
@XmlRootElement
@Data @NoArgsConstructor
public class Paciente implements Serializable {
    private static final long serialVersionUID = -3543130551224881209L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;

    public Paciente(String nome) {
        this.nome = nome;
    }
}
