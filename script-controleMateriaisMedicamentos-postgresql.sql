DELETE FROM materialHospitalar;
DELETE FROM medicamento;

INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (1, 'Termômetro digital', 'Incoterm', 21);
INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (2, 'Envelope auto-selante 90x260mm', 'Vedamax', 400);
INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (3, 'Gaze algodonada 10x15cm', 'Neve', 8200);
INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (4, 'Agulha descartável 0,80x30 21G 1 1/4', 'Injex', 1900);
INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (5, 'Agulha descartável 1,20x40 18G 1 1/2', 'Injex', 1800);
INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (6, 'Luva de vinil', 'Descarpack', 13000);
INSERT INTO materialHospitalar(id, nome, marca, quantidade) VALUES (7, 'Máscara descartável com elástico', 'Descarpack', 5000);

INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (1, 'Tridil 50mg ampola 10ml', 'Cristália', 37);
INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (2, 'Lidocaína 2% sem vasoconstritor injetável 20ml', 'Hypolabor', 45);
INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (3, 'Tramadol injetável', 'União Química', 18);
INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (4, 'Insulina Humana Novolin N 10ml', 'Novo Nordisk', 85);
INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (5, 'Acetilcisteína injetável 10% 3ml 5 ampolas', 'União Química', 23);
INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (6, 'Soro glicofisiológico bolsa', 'Eurofarma', 50);
INSERT INTO medicamento(id, nome, marca, quantidade) VALUES (7, 'Soro Ringer com lactato de sódio bolsa', 'Eurofarma', 65);