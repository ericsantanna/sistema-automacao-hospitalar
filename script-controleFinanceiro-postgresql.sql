DELETE FROM pedidoprocedimentomedico;
DELETE FROM paciente;
DELETE FROM planosaude;

INSERT INTO paciente(id, nome) VALUES (1, 'Ana Almeida');
INSERT INTO paciente(id, nome) VALUES (2, 'Bia Barbosa');
INSERT INTO paciente(id, nome) VALUES (3, 'Carlos Cardoso');
INSERT INTO paciente(id, nome) VALUES (4, 'Diego Dias');
INSERT INTO paciente(id, nome) VALUES (5, 'Edson Esteves');
INSERT INTO paciente(id, nome) VALUES (6, 'Fernanda Farias');
INSERT INTO paciente(id, nome) VALUES (7, 'Glória Galvão');

INSERT INTO planosaude(id, nome) VALUES (1, 'Amil');
INSERT INTO planosaude(id, nome) VALUES (2, 'Bradesco');
INSERT INTO planosaude(id, nome) VALUES (3, 'Porto Seguro');
INSERT INTO planosaude(id, nome) VALUES (4, 'Sul América');
INSERT INTO planosaude(id, nome) VALUES (5, 'Unimed');

INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (1, 2031.88, 1, 1, 'APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (2, 180.00, 1, 1, 'APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (3, 415.90, 2, 2, 'APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (4, 800.00, 3, 2, 'NAO_APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (5, 1036.10, 4, 3, 'APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (6, 250.50, 4, 3, 'APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (7, 4190.13, 5, 4, 'NAO_APROVADO');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (8, 748.28, 6, 5, 'PENDENTE');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (9, 395.14, 6, 5, 'PENDENTE');
INSERT INTO pedidoprocedimentomedico(id, valor, paciente_id, planosaude_id, status)
VALUES (10, 139.00, 7, 5, 'PENDENTE');