package br.pucminas.controleHotelariaHospitalar.webservices;

import br.pucminas.controleHotelariaHospitalar.accessLogger.AccessLoggerInterceptor;
import br.pucminas.controleHotelariaHospitalar.hotelariaHospitalar.Recurso;
import br.pucminas.controleHotelariaHospitalar.hotelariaHospitalar.RecursoDAO;
import br.pucminas.controleHotelariaHospitalar.hotelariaHospitalar.RecursoWS;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.controleHotelariaHospitalar.hotelariaHospitalar.RecursoWS")
public class RecursoWSDefault implements RecursoWS {

    @Inject private RecursoDAO dao;

    @Override
    public Recurso get(long id) {
        return dao.get(id);
    }

    @Override
    public List<Recurso> list() {
        return dao.list();
    }
}
