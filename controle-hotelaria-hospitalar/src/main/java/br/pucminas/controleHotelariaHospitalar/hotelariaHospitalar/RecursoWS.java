package br.pucminas.controleHotelariaHospitalar.hotelariaHospitalar;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface RecursoWS {
    Recurso get(long id);
    List<Recurso> list();
}
