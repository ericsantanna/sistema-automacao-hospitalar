package br.pucminas.controleHotelariaHospitalar.hotelariaHospitalar;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class RecursoDAO {
    @PersistenceContext
    private EntityManager em;

    public Recurso get(long id) {
        return em.find(Recurso.class, id);
    }

    public List<Recurso> list() {
        return em.createQuery("select r from Recurso r", Recurso.class).getResultList();
    }
}
