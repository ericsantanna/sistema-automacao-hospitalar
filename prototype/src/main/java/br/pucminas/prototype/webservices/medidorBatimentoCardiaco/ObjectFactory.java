
package br.pucminas.prototype.webservices.medidorBatimentoCardiaco;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.pucminas.prototype.webservices.medidorBatimentoCardiaco package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Sensor_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "sensor");
    private final static QName _AferirBatimentosCardiaco_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "aferirBatimentosCardiaco");
    private final static QName _BatimentoCardiaco_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "batimentoCardiaco");
    private final static QName _ListResponse_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "listResponse");
    private final static QName _List_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "list");
    private final static QName _AferirBatimentosCardiacoResponse_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "aferirBatimentosCardiacoResponse");
    private final static QName _Get_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "get");
    private final static QName _GetResponse_QNAME = new QName("http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", "getResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.pucminas.prototype.webservices.medidorBatimentoCardiaco
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AferirBatimentosCardiacoResponse }
     * 
     */
    public AferirBatimentosCardiacoResponse createAferirBatimentosCardiacoResponse() {
        return new AferirBatimentosCardiacoResponse();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link AferirBatimentosCardiaco }
     * 
     */
    public AferirBatimentosCardiaco createAferirBatimentosCardiaco() {
        return new AferirBatimentosCardiaco();
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link BatimentoCardiaco }
     * 
     */
    public BatimentoCardiaco createBatimentoCardiaco() {
        return new BatimentoCardiaco();
    }

    /**
     * Create an instance of {@link ListResponse }
     * 
     */
    public ListResponse createListResponse() {
        return new ListResponse();
    }

    /**
     * Create an instance of {@link EquipamentoBatimentoCardiaco }
     * 
     */
    public EquipamentoBatimentoCardiaco createEquipamentoBatimentoCardiaco() {
        return new EquipamentoBatimentoCardiaco();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sensor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "sensor")
    public JAXBElement<Sensor> createSensor(Sensor value) {
        return new JAXBElement<Sensor>(_Sensor_QNAME, Sensor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AferirBatimentosCardiaco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "aferirBatimentosCardiaco")
    public JAXBElement<AferirBatimentosCardiaco> createAferirBatimentosCardiaco(AferirBatimentosCardiaco value) {
        return new JAXBElement<AferirBatimentosCardiaco>(_AferirBatimentosCardiaco_QNAME, AferirBatimentosCardiaco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BatimentoCardiaco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "batimentoCardiaco")
    public JAXBElement<BatimentoCardiaco> createBatimentoCardiaco(BatimentoCardiaco value) {
        return new JAXBElement<BatimentoCardiaco>(_BatimentoCardiaco_QNAME, BatimentoCardiaco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "listResponse")
    public JAXBElement<ListResponse> createListResponse(ListResponse value) {
        return new JAXBElement<ListResponse>(_ListResponse_QNAME, ListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "list")
    public JAXBElement<List> createList(List value) {
        return new JAXBElement<List>(_List_QNAME, List.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AferirBatimentosCardiacoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "aferirBatimentosCardiacoResponse")
    public JAXBElement<AferirBatimentosCardiacoResponse> createAferirBatimentosCardiacoResponse(AferirBatimentosCardiacoResponse value) {
        return new JAXBElement<AferirBatimentosCardiacoResponse>(_AferirBatimentosCardiacoResponse_QNAME, AferirBatimentosCardiacoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://batimentoCardiaco.sensoresInteligentes.pucminas.br/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

}
