
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de hemograma complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="hemograma">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}exame">
 *       &lt;sequence>
 *         &lt;element name="eritograma" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}eritograma" minOccurs="0"/>
 *         &lt;element name="leucograma" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}leucograma" minOccurs="0"/>
 *         &lt;element name="plaqueta" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}plaqueta" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hemograma", propOrder = {
    "eritograma",
    "leucograma",
    "plaqueta"
})
public class Hemograma
    extends Exame
{

    protected Eritograma eritograma;
    protected Leucograma leucograma;
    protected Plaqueta plaqueta;

    /**
     * Obtém o valor da propriedade eritograma.
     * 
     * @return
     *     possible object is
     *     {@link Eritograma }
     *     
     */
    public Eritograma getEritograma() {
        return eritograma;
    }

    /**
     * Define o valor da propriedade eritograma.
     * 
     * @param value
     *     allowed object is
     *     {@link Eritograma }
     *     
     */
    public void setEritograma(Eritograma value) {
        this.eritograma = value;
    }

    /**
     * Obtém o valor da propriedade leucograma.
     * 
     * @return
     *     possible object is
     *     {@link Leucograma }
     *     
     */
    public Leucograma getLeucograma() {
        return leucograma;
    }

    /**
     * Define o valor da propriedade leucograma.
     * 
     * @param value
     *     allowed object is
     *     {@link Leucograma }
     *     
     */
    public void setLeucograma(Leucograma value) {
        this.leucograma = value;
    }

    /**
     * Obtém o valor da propriedade plaqueta.
     * 
     * @return
     *     possible object is
     *     {@link Plaqueta }
     *     
     */
    public Plaqueta getPlaqueta() {
        return plaqueta;
    }

    /**
     * Define o valor da propriedade plaqueta.
     * 
     * @param value
     *     allowed object is
     *     {@link Plaqueta }
     *     
     */
    public void setPlaqueta(Plaqueta value) {
        this.plaqueta = value;
    }

}
