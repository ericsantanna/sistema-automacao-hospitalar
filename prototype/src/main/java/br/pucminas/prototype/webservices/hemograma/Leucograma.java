
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de leucograma complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="leucograma">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="leucocito" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}leucocito" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "leucograma", propOrder = {
    "leucocito"
})
public class Leucograma {

    protected Leucocito leucocito;

    /**
     * Obtém o valor da propriedade leucocito.
     * 
     * @return
     *     possible object is
     *     {@link Leucocito }
     *     
     */
    public Leucocito getLeucocito() {
        return leucocito;
    }

    /**
     * Define o valor da propriedade leucocito.
     * 
     * @param value
     *     allowed object is
     *     {@link Leucocito }
     *     
     */
    public void setLeucocito(Leucocito value) {
        this.leucocito = value;
    }

}
