
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de neutrofilo complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="neutrofilo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}medida">
 *       &lt;sequence>
 *         &lt;element name="metamielocito" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}metamielocito" minOccurs="0"/>
 *         &lt;element name="mielocito" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}mielocito" minOccurs="0"/>
 *         &lt;element name="segmentado" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}segmentado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "neutrofilo", propOrder = {
    "metamielocito",
    "mielocito",
    "segmentado"
})
public class Neutrofilo
    extends Medida
{

    protected Metamielocito metamielocito;
    protected Mielocito mielocito;
    protected Segmentado segmentado;

    /**
     * Obtém o valor da propriedade metamielocito.
     * 
     * @return
     *     possible object is
     *     {@link Metamielocito }
     *     
     */
    public Metamielocito getMetamielocito() {
        return metamielocito;
    }

    /**
     * Define o valor da propriedade metamielocito.
     * 
     * @param value
     *     allowed object is
     *     {@link Metamielocito }
     *     
     */
    public void setMetamielocito(Metamielocito value) {
        this.metamielocito = value;
    }

    /**
     * Obtém o valor da propriedade mielocito.
     * 
     * @return
     *     possible object is
     *     {@link Mielocito }
     *     
     */
    public Mielocito getMielocito() {
        return mielocito;
    }

    /**
     * Define o valor da propriedade mielocito.
     * 
     * @param value
     *     allowed object is
     *     {@link Mielocito }
     *     
     */
    public void setMielocito(Mielocito value) {
        this.mielocito = value;
    }

    /**
     * Obtém o valor da propriedade segmentado.
     * 
     * @return
     *     possible object is
     *     {@link Segmentado }
     *     
     */
    public Segmentado getSegmentado() {
        return segmentado;
    }

    /**
     * Define o valor da propriedade segmentado.
     * 
     * @param value
     *     allowed object is
     *     {@link Segmentado }
     *     
     */
    public void setSegmentado(Segmentado value) {
        this.segmentado = value;
    }

}
