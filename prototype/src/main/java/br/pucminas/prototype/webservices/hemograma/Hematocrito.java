
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de hematocrito complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="hematocrito">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}medida">
 *       &lt;sequence>
 *         &lt;element name="chcm" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}chcm" minOccurs="0"/>
 *         &lt;element name="hcm" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}hcm" minOccurs="0"/>
 *         &lt;element name="rdw" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}rdw" minOccurs="0"/>
 *         &lt;element name="vcm" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}vcm" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hematocrito", propOrder = {
    "chcm",
    "hcm",
    "rdw",
    "vcm"
})
public class Hematocrito
    extends Medida
{

    protected Chcm chcm;
    protected Hcm hcm;
    protected Rdw rdw;
    protected Vcm vcm;

    /**
     * Obtém o valor da propriedade chcm.
     * 
     * @return
     *     possible object is
     *     {@link Chcm }
     *     
     */
    public Chcm getChcm() {
        return chcm;
    }

    /**
     * Define o valor da propriedade chcm.
     * 
     * @param value
     *     allowed object is
     *     {@link Chcm }
     *     
     */
    public void setChcm(Chcm value) {
        this.chcm = value;
    }

    /**
     * Obtém o valor da propriedade hcm.
     * 
     * @return
     *     possible object is
     *     {@link Hcm }
     *     
     */
    public Hcm getHcm() {
        return hcm;
    }

    /**
     * Define o valor da propriedade hcm.
     * 
     * @param value
     *     allowed object is
     *     {@link Hcm }
     *     
     */
    public void setHcm(Hcm value) {
        this.hcm = value;
    }

    /**
     * Obtém o valor da propriedade rdw.
     * 
     * @return
     *     possible object is
     *     {@link Rdw }
     *     
     */
    public Rdw getRdw() {
        return rdw;
    }

    /**
     * Define o valor da propriedade rdw.
     * 
     * @param value
     *     allowed object is
     *     {@link Rdw }
     *     
     */
    public void setRdw(Rdw value) {
        this.rdw = value;
    }

    /**
     * Obtém o valor da propriedade vcm.
     * 
     * @return
     *     possible object is
     *     {@link Vcm }
     *     
     */
    public Vcm getVcm() {
        return vcm;
    }

    /**
     * Define o valor da propriedade vcm.
     * 
     * @param value
     *     allowed object is
     *     {@link Vcm }
     *     
     */
    public void setVcm(Vcm value) {
        this.vcm = value;
    }

}
