
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de linfocitoAtipico complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="linfocitoAtipico">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}medida">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linfocitoAtipico")
public class LinfocitoAtipico
    extends Medida
{


}
