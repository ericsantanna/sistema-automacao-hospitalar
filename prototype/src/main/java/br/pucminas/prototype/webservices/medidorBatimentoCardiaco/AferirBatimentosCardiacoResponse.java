
package br.pucminas.prototype.webservices.medidorBatimentoCardiaco;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de aferirBatimentosCardiacoResponse complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="aferirBatimentosCardiacoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://batimentoCardiaco.sensoresInteligentes.pucminas.br/}batimentoCardiaco" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aferirBatimentosCardiacoResponse", propOrder = {
    "_return"
})
public class AferirBatimentosCardiacoResponse {

    @XmlElement(name = "return")
    protected BatimentoCardiaco _return;

    /**
     * Obtém o valor da propriedade return.
     * 
     * @return
     *     possible object is
     *     {@link BatimentoCardiaco }
     *     
     */
    public BatimentoCardiaco getReturn() {
        return _return;
    }

    /**
     * Define o valor da propriedade return.
     * 
     * @param value
     *     allowed object is
     *     {@link BatimentoCardiaco }
     *     
     */
    public void setReturn(BatimentoCardiaco value) {
        this._return = value;
    }

}
