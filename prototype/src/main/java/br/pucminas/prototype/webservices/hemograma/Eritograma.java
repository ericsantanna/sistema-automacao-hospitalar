
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de eritograma complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="eritograma">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eritocito" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}eritocito" minOccurs="0"/>
 *         &lt;element name="hematocrito" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}hematocrito" minOccurs="0"/>
 *         &lt;element name="hemoglobina" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}hemoglobina" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eritograma", propOrder = {
    "eritocito",
    "hematocrito",
    "hemoglobina"
})
public class Eritograma {

    protected Eritocito eritocito;
    protected Hematocrito hematocrito;
    protected Hemoglobina hemoglobina;

    /**
     * Obtém o valor da propriedade eritocito.
     * 
     * @return
     *     possible object is
     *     {@link Eritocito }
     *     
     */
    public Eritocito getEritocito() {
        return eritocito;
    }

    /**
     * Define o valor da propriedade eritocito.
     * 
     * @param value
     *     allowed object is
     *     {@link Eritocito }
     *     
     */
    public void setEritocito(Eritocito value) {
        this.eritocito = value;
    }

    /**
     * Obtém o valor da propriedade hematocrito.
     * 
     * @return
     *     possible object is
     *     {@link Hematocrito }
     *     
     */
    public Hematocrito getHematocrito() {
        return hematocrito;
    }

    /**
     * Define o valor da propriedade hematocrito.
     * 
     * @param value
     *     allowed object is
     *     {@link Hematocrito }
     *     
     */
    public void setHematocrito(Hematocrito value) {
        this.hematocrito = value;
    }

    /**
     * Obtém o valor da propriedade hemoglobina.
     * 
     * @return
     *     possible object is
     *     {@link Hemoglobina }
     *     
     */
    public Hemoglobina getHemoglobina() {
        return hemoglobina;
    }

    /**
     * Define o valor da propriedade hemoglobina.
     * 
     * @param value
     *     allowed object is
     *     {@link Hemoglobina }
     *     
     */
    public void setHemoglobina(Hemoglobina value) {
        this.hemoglobina = value;
    }

}
