
package br.pucminas.prototype.webservices.medidorPressaoArterial;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de aferirPressaoArterialResponse complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="aferirPressaoArterialResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://pressaoArterial.sensoresInteligentes.pucminas.br/}pressaoArterial" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aferirPressaoArterialResponse", propOrder = {
    "_return"
})
public class AferirPressaoArterialResponse {

    @XmlElement(name = "return")
    protected PressaoArterial _return;

    /**
     * Obtém o valor da propriedade return.
     * 
     * @return
     *     possible object is
     *     {@link PressaoArterial }
     *     
     */
    public PressaoArterial getReturn() {
        return _return;
    }

    /**
     * Define o valor da propriedade return.
     * 
     * @param value
     *     allowed object is
     *     {@link PressaoArterial }
     *     
     */
    public void setReturn(PressaoArterial value) {
        this._return = value;
    }

}
