
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de leucocito complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="leucocito">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}medida">
 *       &lt;sequence>
 *         &lt;element name="basofilo" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}basofilo" minOccurs="0"/>
 *         &lt;element name="bastonete" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}bastonete" minOccurs="0"/>
 *         &lt;element name="eosinofilo" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}eosinofilo" minOccurs="0"/>
 *         &lt;element name="linfocitoAtipico" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}linfocitoAtipico" minOccurs="0"/>
 *         &lt;element name="linfocitoTipico" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}linfocitoTipico" minOccurs="0"/>
 *         &lt;element name="monocito" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}monocito" minOccurs="0"/>
 *         &lt;element name="neutrofilo" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}neutrofilo" minOccurs="0"/>
 *         &lt;element name="outro" type="{http://hemograma.sangue.exames.laboratorioExames.pucminas.br/}outro" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "leucocito", propOrder = {
    "basofilo",
    "bastonete",
    "eosinofilo",
    "linfocitoAtipico",
    "linfocitoTipico",
    "monocito",
    "neutrofilo",
    "outro"
})
public class Leucocito
    extends Medida
{

    protected Basofilo basofilo;
    protected Bastonete bastonete;
    protected Eosinofilo eosinofilo;
    protected LinfocitoAtipico linfocitoAtipico;
    protected LinfocitoTipico linfocitoTipico;
    protected Monocito monocito;
    protected Neutrofilo neutrofilo;
    protected Outro outro;

    /**
     * Obtém o valor da propriedade basofilo.
     * 
     * @return
     *     possible object is
     *     {@link Basofilo }
     *     
     */
    public Basofilo getBasofilo() {
        return basofilo;
    }

    /**
     * Define o valor da propriedade basofilo.
     * 
     * @param value
     *     allowed object is
     *     {@link Basofilo }
     *     
     */
    public void setBasofilo(Basofilo value) {
        this.basofilo = value;
    }

    /**
     * Obtém o valor da propriedade bastonete.
     * 
     * @return
     *     possible object is
     *     {@link Bastonete }
     *     
     */
    public Bastonete getBastonete() {
        return bastonete;
    }

    /**
     * Define o valor da propriedade bastonete.
     * 
     * @param value
     *     allowed object is
     *     {@link Bastonete }
     *     
     */
    public void setBastonete(Bastonete value) {
        this.bastonete = value;
    }

    /**
     * Obtém o valor da propriedade eosinofilo.
     * 
     * @return
     *     possible object is
     *     {@link Eosinofilo }
     *     
     */
    public Eosinofilo getEosinofilo() {
        return eosinofilo;
    }

    /**
     * Define o valor da propriedade eosinofilo.
     * 
     * @param value
     *     allowed object is
     *     {@link Eosinofilo }
     *     
     */
    public void setEosinofilo(Eosinofilo value) {
        this.eosinofilo = value;
    }

    /**
     * Obtém o valor da propriedade linfocitoAtipico.
     * 
     * @return
     *     possible object is
     *     {@link LinfocitoAtipico }
     *     
     */
    public LinfocitoAtipico getLinfocitoAtipico() {
        return linfocitoAtipico;
    }

    /**
     * Define o valor da propriedade linfocitoAtipico.
     * 
     * @param value
     *     allowed object is
     *     {@link LinfocitoAtipico }
     *     
     */
    public void setLinfocitoAtipico(LinfocitoAtipico value) {
        this.linfocitoAtipico = value;
    }

    /**
     * Obtém o valor da propriedade linfocitoTipico.
     * 
     * @return
     *     possible object is
     *     {@link LinfocitoTipico }
     *     
     */
    public LinfocitoTipico getLinfocitoTipico() {
        return linfocitoTipico;
    }

    /**
     * Define o valor da propriedade linfocitoTipico.
     * 
     * @param value
     *     allowed object is
     *     {@link LinfocitoTipico }
     *     
     */
    public void setLinfocitoTipico(LinfocitoTipico value) {
        this.linfocitoTipico = value;
    }

    /**
     * Obtém o valor da propriedade monocito.
     * 
     * @return
     *     possible object is
     *     {@link Monocito }
     *     
     */
    public Monocito getMonocito() {
        return monocito;
    }

    /**
     * Define o valor da propriedade monocito.
     * 
     * @param value
     *     allowed object is
     *     {@link Monocito }
     *     
     */
    public void setMonocito(Monocito value) {
        this.monocito = value;
    }

    /**
     * Obtém o valor da propriedade neutrofilo.
     * 
     * @return
     *     possible object is
     *     {@link Neutrofilo }
     *     
     */
    public Neutrofilo getNeutrofilo() {
        return neutrofilo;
    }

    /**
     * Define o valor da propriedade neutrofilo.
     * 
     * @param value
     *     allowed object is
     *     {@link Neutrofilo }
     *     
     */
    public void setNeutrofilo(Neutrofilo value) {
        this.neutrofilo = value;
    }

    /**
     * Obtém o valor da propriedade outro.
     * 
     * @return
     *     possible object is
     *     {@link Outro }
     *     
     */
    public Outro getOutro() {
        return outro;
    }

    /**
     * Define o valor da propriedade outro.
     * 
     * @param value
     *     allowed object is
     *     {@link Outro }
     *     
     */
    public void setOutro(Outro value) {
        this.outro = value;
    }

}
