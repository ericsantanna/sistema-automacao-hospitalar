
package br.pucminas.prototype.webservices.medidorPressaoArterial;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de equipamentoPressaoArterial complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="equipamentoPressaoArterial">
 *   &lt;complexContent>
 *     &lt;extension base="{http://pressaoArterial.sensoresInteligentes.pucminas.br/}sensor">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "equipamentoPressaoArterial")
public class EquipamentoPressaoArterial
    extends Sensor
{


}
