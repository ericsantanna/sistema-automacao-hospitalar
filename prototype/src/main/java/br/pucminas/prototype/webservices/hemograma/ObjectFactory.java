
package br.pucminas.prototype.webservices.hemograma;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.pucminas.prototype.webservices.hemograma package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Get_QNAME = new QName("http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", "get");
    private final static QName _GetResponse_QNAME = new QName("http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", "getResponse");
    private final static QName _Paciente_QNAME = new QName("http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", "paciente");
    private final static QName _Exame_QNAME = new QName("http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", "exame");
    private final static QName _List_QNAME = new QName("http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", "list");
    private final static QName _ListResponse_QNAME = new QName("http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", "listResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.pucminas.prototype.webservices.hemograma
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link Paciente }
     * 
     */
    public Paciente createPaciente() {
        return new Paciente();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link ListResponse }
     * 
     */
    public ListResponse createListResponse() {
        return new ListResponse();
    }

    /**
     * Create an instance of {@link Leucocito }
     * 
     */
    public Leucocito createLeucocito() {
        return new Leucocito();
    }

    /**
     * Create an instance of {@link Rdw }
     * 
     */
    public Rdw createRdw() {
        return new Rdw();
    }

    /**
     * Create an instance of {@link Bastonete }
     * 
     */
    public Bastonete createBastonete() {
        return new Bastonete();
    }

    /**
     * Create an instance of {@link Neutrofilo }
     * 
     */
    public Neutrofilo createNeutrofilo() {
        return new Neutrofilo();
    }

    /**
     * Create an instance of {@link Mielocito }
     * 
     */
    public Mielocito createMielocito() {
        return new Mielocito();
    }

    /**
     * Create an instance of {@link Eritocito }
     * 
     */
    public Eritocito createEritocito() {
        return new Eritocito();
    }

    /**
     * Create an instance of {@link Hcm }
     * 
     */
    public Hcm createHcm() {
        return new Hcm();
    }

    /**
     * Create an instance of {@link Eritograma }
     * 
     */
    public Eritograma createEritograma() {
        return new Eritograma();
    }

    /**
     * Create an instance of {@link Basofilo }
     * 
     */
    public Basofilo createBasofilo() {
        return new Basofilo();
    }

    /**
     * Create an instance of {@link Outro }
     * 
     */
    public Outro createOutro() {
        return new Outro();
    }

    /**
     * Create an instance of {@link Leucograma }
     * 
     */
    public Leucograma createLeucograma() {
        return new Leucograma();
    }

    /**
     * Create an instance of {@link LinfocitoTipico }
     * 
     */
    public LinfocitoTipico createLinfocitoTipico() {
        return new LinfocitoTipico();
    }

    /**
     * Create an instance of {@link Hemograma }
     * 
     */
    public Hemograma createHemograma() {
        return new Hemograma();
    }

    /**
     * Create an instance of {@link Chcm }
     * 
     */
    public Chcm createChcm() {
        return new Chcm();
    }

    /**
     * Create an instance of {@link Vcm }
     * 
     */
    public Vcm createVcm() {
        return new Vcm();
    }

    /**
     * Create an instance of {@link Segmentado }
     * 
     */
    public Segmentado createSegmentado() {
        return new Segmentado();
    }

    /**
     * Create an instance of {@link LinfocitoAtipico }
     * 
     */
    public LinfocitoAtipico createLinfocitoAtipico() {
        return new LinfocitoAtipico();
    }

    /**
     * Create an instance of {@link Plaqueta }
     * 
     */
    public Plaqueta createPlaqueta() {
        return new Plaqueta();
    }

    /**
     * Create an instance of {@link Hematocrito }
     * 
     */
    public Hematocrito createHematocrito() {
        return new Hematocrito();
    }

    /**
     * Create an instance of {@link Monocito }
     * 
     */
    public Monocito createMonocito() {
        return new Monocito();
    }

    /**
     * Create an instance of {@link Metamielocito }
     * 
     */
    public Metamielocito createMetamielocito() {
        return new Metamielocito();
    }

    /**
     * Create an instance of {@link Hemoglobina }
     * 
     */
    public Hemoglobina createHemoglobina() {
        return new Hemoglobina();
    }

    /**
     * Create an instance of {@link Eosinofilo }
     * 
     */
    public Eosinofilo createEosinofilo() {
        return new Eosinofilo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Paciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", name = "paciente")
    public JAXBElement<Paciente> createPaciente(Paciente value) {
        return new JAXBElement<Paciente>(_Paciente_QNAME, Paciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exame }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", name = "exame")
    public JAXBElement<Exame> createExame(Exame value) {
        return new JAXBElement<Exame>(_Exame_QNAME, Exame.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", name = "list")
    public JAXBElement<List> createList(List value) {
        return new JAXBElement<List>(_List_QNAME, List.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hemograma.sangue.exames.laboratorioExames.pucminas.br/", name = "listResponse")
    public JAXBElement<ListResponse> createListResponse(ListResponse value) {
        return new JAXBElement<ListResponse>(_ListResponse_QNAME, ListResponse.class, null, value);
    }

}
