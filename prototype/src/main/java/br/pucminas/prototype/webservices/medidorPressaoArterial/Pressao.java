
package br.pucminas.prototype.webservices.medidorPressaoArterial;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de pressao.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="pressao">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MMHG"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "pressao")
@XmlEnum
public enum Pressao {

    MMHG;

    public String value() {
        return name();
    }

    public static Pressao fromValue(String v) {
        return valueOf(v);
    }

}
