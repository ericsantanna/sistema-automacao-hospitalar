
package br.pucminas.prototype.webservices.medidorBatimentoCardiaco;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de batimentoCardiaco complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="batimentoCardiaco">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="batimentosPorMinuto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "batimentoCardiaco", propOrder = {
    "batimentosPorMinuto"
})
public class BatimentoCardiaco {

    protected int batimentosPorMinuto;

    /**
     * Obtém o valor da propriedade batimentosPorMinuto.
     * 
     */
    public int getBatimentosPorMinuto() {
        return batimentosPorMinuto;
    }

    /**
     * Define o valor da propriedade batimentosPorMinuto.
     * 
     */
    public void setBatimentosPorMinuto(int value) {
        this.batimentosPorMinuto = value;
    }

}
