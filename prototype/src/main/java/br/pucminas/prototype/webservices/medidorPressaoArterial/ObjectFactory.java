
package br.pucminas.prototype.webservices.medidorPressaoArterial;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.pucminas.prototype.webservices.medidorPressaoArterial package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AferirPressaoArterialResponse_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "aferirPressaoArterialResponse");
    private final static QName _Sensor_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "sensor");
    private final static QName _ListResponse_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "listResponse");
    private final static QName _List_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "list");
    private final static QName _PressaoArterial_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "pressaoArterial");
    private final static QName _AferirPressaoArterial_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "aferirPressaoArterial");
    private final static QName _Get_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "get");
    private final static QName _GetResponse_QNAME = new QName("http://pressaoArterial.sensoresInteligentes.pucminas.br/", "getResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.pucminas.prototype.webservices.medidorPressaoArterial
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AferirPressaoArterial }
     * 
     */
    public AferirPressaoArterial createAferirPressaoArterial() {
        return new AferirPressaoArterial();
    }

    /**
     * Create an instance of {@link PressaoArterial }
     * 
     */
    public PressaoArterial createPressaoArterial() {
        return new PressaoArterial();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link AferirPressaoArterialResponse }
     * 
     */
    public AferirPressaoArterialResponse createAferirPressaoArterialResponse() {
        return new AferirPressaoArterialResponse();
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link ListResponse }
     * 
     */
    public ListResponse createListResponse() {
        return new ListResponse();
    }

    /**
     * Create an instance of {@link EquipamentoPressaoArterial }
     * 
     */
    public EquipamentoPressaoArterial createEquipamentoPressaoArterial() {
        return new EquipamentoPressaoArterial();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AferirPressaoArterialResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "aferirPressaoArterialResponse")
    public JAXBElement<AferirPressaoArterialResponse> createAferirPressaoArterialResponse(AferirPressaoArterialResponse value) {
        return new JAXBElement<AferirPressaoArterialResponse>(_AferirPressaoArterialResponse_QNAME, AferirPressaoArterialResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Sensor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "sensor")
    public JAXBElement<Sensor> createSensor(Sensor value) {
        return new JAXBElement<Sensor>(_Sensor_QNAME, Sensor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "listResponse")
    public JAXBElement<ListResponse> createListResponse(ListResponse value) {
        return new JAXBElement<ListResponse>(_ListResponse_QNAME, ListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "list")
    public JAXBElement<List> createList(List value) {
        return new JAXBElement<List>(_List_QNAME, List.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PressaoArterial }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "pressaoArterial")
    public JAXBElement<PressaoArterial> createPressaoArterial(PressaoArterial value) {
        return new JAXBElement<PressaoArterial>(_PressaoArterial_QNAME, PressaoArterial.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AferirPressaoArterial }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "aferirPressaoArterial")
    public JAXBElement<AferirPressaoArterial> createAferirPressaoArterial(AferirPressaoArterial value) {
        return new JAXBElement<AferirPressaoArterial>(_AferirPressaoArterial_QNAME, AferirPressaoArterial.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://pressaoArterial.sensoresInteligentes.pucminas.br/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

}
