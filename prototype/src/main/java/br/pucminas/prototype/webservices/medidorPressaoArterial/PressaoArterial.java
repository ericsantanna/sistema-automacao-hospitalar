
package br.pucminas.prototype.webservices.medidorPressaoArterial;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de pressaoArterial complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="pressaoArterial">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="diastolica" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sistolica" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="unidade" type="{http://pressaoArterial.sensoresInteligentes.pucminas.br/}pressao" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pressaoArterial", propOrder = {
    "diastolica",
    "sistolica",
    "unidade"
})
public class PressaoArterial {

    protected int diastolica;
    protected int sistolica;
    @XmlSchemaType(name = "string")
    protected Pressao unidade;

    /**
     * Obtém o valor da propriedade diastolica.
     * 
     */
    public int getDiastolica() {
        return diastolica;
    }

    /**
     * Define o valor da propriedade diastolica.
     * 
     */
    public void setDiastolica(int value) {
        this.diastolica = value;
    }

    /**
     * Obtém o valor da propriedade sistolica.
     * 
     */
    public int getSistolica() {
        return sistolica;
    }

    /**
     * Define o valor da propriedade sistolica.
     * 
     */
    public void setSistolica(int value) {
        this.sistolica = value;
    }

    /**
     * Obtém o valor da propriedade unidade.
     * 
     * @return
     *     possible object is
     *     {@link Pressao }
     *     
     */
    public Pressao getUnidade() {
        return unidade;
    }

    /**
     * Define o valor da propriedade unidade.
     * 
     * @param value
     *     allowed object is
     *     {@link Pressao }
     *     
     */
    public void setUnidade(Pressao value) {
        this.unidade = value;
    }

}
