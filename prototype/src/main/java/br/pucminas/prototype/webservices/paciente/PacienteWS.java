
package br.pucminas.prototype.webservices.paciente;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "PacienteWS", targetNamespace = "http://paciente.controleFinanceiro.pucminas.br/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface PacienteWS {


    /**
     * 
     * @param arg0
     * @return
     *     returns br.pucminas.prototype.webservices.paciente.Paciente
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "get", targetNamespace = "http://paciente.controleFinanceiro.pucminas.br/", className = "br.pucminas.prototype.webservices.paciente.Get")
    @ResponseWrapper(localName = "getResponse", targetNamespace = "http://paciente.controleFinanceiro.pucminas.br/", className = "br.pucminas.prototype.webservices.paciente.GetResponse")
    public Paciente get(
        @WebParam(name = "arg0", targetNamespace = "")
        long arg0);

}
