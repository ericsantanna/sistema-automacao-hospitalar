
package br.pucminas.prototype.webservices.planoSaude;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "PlanoSaudeWSDefaultService", targetNamespace = "http://webservices.controleFinanceiro.pucminas.br/", wsdlLocation = "http://localhost:8083/PlanoSaudeWSDefault?wsdl")
public class PlanoSaudeWSDefaultService
    extends Service
{

    private final static URL PLANOSAUDEWSDEFAULTSERVICE_WSDL_LOCATION;
    private final static WebServiceException PLANOSAUDEWSDEFAULTSERVICE_EXCEPTION;
    private final static QName PLANOSAUDEWSDEFAULTSERVICE_QNAME = new QName("http://webservices.controleFinanceiro.pucminas.br/", "PlanoSaudeWSDefaultService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8083/PlanoSaudeWSDefault?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        PLANOSAUDEWSDEFAULTSERVICE_WSDL_LOCATION = url;
        PLANOSAUDEWSDEFAULTSERVICE_EXCEPTION = e;
    }

    public PlanoSaudeWSDefaultService() {
        super(__getWsdlLocation(), PLANOSAUDEWSDEFAULTSERVICE_QNAME);
    }

    public PlanoSaudeWSDefaultService(WebServiceFeature... features) {
        super(__getWsdlLocation(), PLANOSAUDEWSDEFAULTSERVICE_QNAME, features);
    }

    public PlanoSaudeWSDefaultService(URL wsdlLocation) {
        super(wsdlLocation, PLANOSAUDEWSDEFAULTSERVICE_QNAME);
    }

    public PlanoSaudeWSDefaultService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, PLANOSAUDEWSDEFAULTSERVICE_QNAME, features);
    }

    public PlanoSaudeWSDefaultService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public PlanoSaudeWSDefaultService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PlanoSaudeWS
     */
    @WebEndpoint(name = "PlanoSaudeWSDefaultPort")
    public PlanoSaudeWS getPlanoSaudeWSDefaultPort() {
        return super.getPort(new QName("http://webservices.controleFinanceiro.pucminas.br/", "PlanoSaudeWSDefaultPort"), PlanoSaudeWS.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PlanoSaudeWS
     */
    @WebEndpoint(name = "PlanoSaudeWSDefaultPort")
    public PlanoSaudeWS getPlanoSaudeWSDefaultPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://webservices.controleFinanceiro.pucminas.br/", "PlanoSaudeWSDefaultPort"), PlanoSaudeWS.class, features);
    }

    private static URL __getWsdlLocation() {
        if (PLANOSAUDEWSDEFAULTSERVICE_EXCEPTION!= null) {
            throw PLANOSAUDEWSDEFAULTSERVICE_EXCEPTION;
        }
        return PLANOSAUDEWSDEFAULTSERVICE_WSDL_LOCATION;
    }

}
