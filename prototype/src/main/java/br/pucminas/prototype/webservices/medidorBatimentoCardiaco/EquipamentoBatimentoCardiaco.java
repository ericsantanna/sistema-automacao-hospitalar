
package br.pucminas.prototype.webservices.medidorBatimentoCardiaco;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de equipamentoBatimentoCardiaco complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="equipamentoBatimentoCardiaco">
 *   &lt;complexContent>
 *     &lt;extension base="{http://batimentoCardiaco.sensoresInteligentes.pucminas.br/}sensor">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "equipamentoBatimentoCardiaco")
public class EquipamentoBatimentoCardiaco
    extends Sensor
{


}
