
package br.pucminas.prototype.webservices.pedidoProcedimentoMedico;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de pedidoProcedimentoMedico complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="pedidoProcedimentoMedico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="paciente" type="{http://pedidoProcedimentoMedico.controleFinanceiro.pucminas.br/}paciente" minOccurs="0"/>
 *         &lt;element name="planoSaude" type="{http://pedidoProcedimentoMedico.controleFinanceiro.pucminas.br/}planoSaude" minOccurs="0"/>
 *         &lt;element name="status" type="{http://pedidoProcedimentoMedico.controleFinanceiro.pucminas.br/}status" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pedidoProcedimentoMedico", propOrder = {
    "id",
    "paciente",
    "planoSaude",
    "status",
    "valor"
})
public class PedidoProcedimentoMedico {

    protected long id;
    protected Paciente paciente;
    protected PlanoSaude planoSaude;
    @XmlSchemaType(name = "string")
    protected Status status;
    protected BigDecimal valor;

    /**
     * Obtém o valor da propriedade id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtém o valor da propriedade paciente.
     * 
     * @return
     *     possible object is
     *     {@link Paciente }
     *     
     */
    public Paciente getPaciente() {
        return paciente;
    }

    /**
     * Define o valor da propriedade paciente.
     * 
     * @param value
     *     allowed object is
     *     {@link Paciente }
     *     
     */
    public void setPaciente(Paciente value) {
        this.paciente = value;
    }

    /**
     * Obtém o valor da propriedade planoSaude.
     * 
     * @return
     *     possible object is
     *     {@link PlanoSaude }
     *     
     */
    public PlanoSaude getPlanoSaude() {
        return planoSaude;
    }

    /**
     * Define o valor da propriedade planoSaude.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanoSaude }
     *     
     */
    public void setPlanoSaude(PlanoSaude value) {
        this.planoSaude = value;
    }

    /**
     * Obtém o valor da propriedade status.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Define o valor da propriedade status.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

    /**
     * Obtém o valor da propriedade valor.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Define o valor da propriedade valor.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

}
