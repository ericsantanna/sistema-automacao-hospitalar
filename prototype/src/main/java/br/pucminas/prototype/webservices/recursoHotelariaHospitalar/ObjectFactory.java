
package br.pucminas.prototype.webservices.recursoHotelariaHospitalar;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.pucminas.prototype.webservices.recursoHotelariaHospitalar package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Recurso_QNAME = new QName("http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", "recurso");
    private final static QName _Get_QNAME = new QName("http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", "get");
    private final static QName _GetResponse_QNAME = new QName("http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", "getResponse");
    private final static QName _List_QNAME = new QName("http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", "list");
    private final static QName _ListResponse_QNAME = new QName("http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", "listResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.pucminas.prototype.webservices.recursoHotelariaHospitalar
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Recurso }
     * 
     */
    public Recurso createRecurso() {
        return new Recurso();
    }

    /**
     * Create an instance of {@link GetResponse }
     * 
     */
    public GetResponse createGetResponse() {
        return new GetResponse();
    }

    /**
     * Create an instance of {@link Get }
     * 
     */
    public Get createGet() {
        return new Get();
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link ListResponse }
     * 
     */
    public ListResponse createListResponse() {
        return new ListResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Recurso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", name = "recurso")
    public JAXBElement<Recurso> createRecurso(Recurso value) {
        return new JAXBElement<Recurso>(_Recurso_QNAME, Recurso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Get }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", name = "get")
    public JAXBElement<Get> createGet(Get value) {
        return new JAXBElement<Get>(_Get_QNAME, Get.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", name = "getResponse")
    public JAXBElement<GetResponse> createGetResponse(GetResponse value) {
        return new JAXBElement<GetResponse>(_GetResponse_QNAME, GetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", name = "list")
    public JAXBElement<List> createList(List value) {
        return new JAXBElement<List>(_List_QNAME, List.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotelariaHospitalar.controleHotelariaHospitalar.pucminas.br/", name = "listResponse")
    public JAXBElement<ListResponse> createListResponse(ListResponse value) {
        return new JAXBElement<ListResponse>(_ListResponse_QNAME, ListResponse.class, null, value);
    }

}
