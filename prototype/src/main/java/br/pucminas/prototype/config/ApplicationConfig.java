package br.pucminas.prototype.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Eric Sant'Anna
 */
@ApplicationPath("api")
public class ApplicationConfig extends Application {}
