package br.pucminas.prototype.controllers;

import br.pucminas.prototype.util.WebserviceUtils;
import br.pucminas.prototype.webservices.hemograma.Hemograma;
import br.pucminas.prototype.webservices.hemograma.HemogramaWS;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
@Path("hemogramas")
@Produces(MediaType.APPLICATION_JSON)
public class HemogramaController {

    @Inject private WebserviceUtils webserviceUtils;

    private HemogramaWS getHemogramaWS() throws MalformedURLException {
        return (HemogramaWS) webserviceUtils.getService("hemogramaWS", HemogramaWS.class);
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") long id) throws JsonProcessingException, MalformedURLException {
        HemogramaWS hemogramaWS = getHemogramaWS();
        Hemograma hemograma = hemogramaWS.get(id);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        String json = objectMapper.writeValueAsString(hemograma);

        return Response.ok(json).build();
    }

    @GET
    @Wrapped(element = "hemogramas")
    public Response list() throws JsonProcessingException, MalformedURLException {
        HemogramaWS hemogramaWS = getHemogramaWS();
        List<Hemograma> list = hemogramaWS.list();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        String json = objectMapper.writeValueAsString(list);

        return Response.ok(json).build();
    }
}
