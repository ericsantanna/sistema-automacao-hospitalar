package br.pucminas.prototype.controllers;

import br.pucminas.prototype.util.WebserviceUtils;
import br.pucminas.prototype.webservices.materialHospitalar.MaterialHospitalar;
import br.pucminas.prototype.webservices.materialHospitalar.MaterialHospitalarWS;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
@Path("materiaisHospitalares")
@Produces(MediaType.APPLICATION_JSON)
public class MaterialHospitalarController {

    @Inject private WebserviceUtils webserviceUtils;

    private MaterialHospitalarWS getMaterialHospitalarWS() throws MalformedURLException {
        return (MaterialHospitalarWS) webserviceUtils.getService("materialHospitalarWS", MaterialHospitalarWS.class);
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") long id) throws MalformedURLException {
        MaterialHospitalarWS materialHospitalarWS = getMaterialHospitalarWS();
        return Response.ok(materialHospitalarWS.get(id)).build();
    }

    @GET
    @Wrapped(element = "materiaisHospitalares")
    public Response list() throws MalformedURLException {
        MaterialHospitalarWS materialHospitalarWS = getMaterialHospitalarWS();
        List<MaterialHospitalar> list = materialHospitalarWS.list();
        return Response.ok(new GenericEntity<List<MaterialHospitalar>>(list) {}).build();
    }
}
