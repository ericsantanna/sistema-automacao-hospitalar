package br.pucminas.prototype.controllers;

import br.pucminas.prototype.util.WebserviceUtils;
import br.pucminas.prototype.webservices.recursoHotelariaHospitalar.Recurso;
import br.pucminas.prototype.webservices.recursoHotelariaHospitalar.RecursoWS;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
@Path("recursosHotelariaHospilatar")
@Produces(MediaType.APPLICATION_JSON)
public class ControleHotelariaHospitalarController {

    @Inject private WebserviceUtils webserviceUtils;

    private RecursoWS getRecursoWS() throws MalformedURLException {
        return (RecursoWS) webserviceUtils.getService("recursoWS", RecursoWS.class);
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") long id) throws MalformedURLException {
        RecursoWS recursoWS = getRecursoWS();
        return Response.ok(recursoWS.get(id)).build();
    }

    @GET
    @Wrapped(element = "recursos")
    public Response list() throws MalformedURLException {
        RecursoWS recursoWS = getRecursoWS();
        List<Recurso> list = recursoWS.list();
        return Response.ok(new GenericEntity<List<Recurso>>(list) {}).build();
    }
}
