package br.pucminas.prototype.controllers;

import br.pucminas.prototype.util.WebserviceUtils;
import br.pucminas.prototype.webservices.pedidoProcedimentoMedico.PedidoProcedimentoMedico;
import br.pucminas.prototype.webservices.pedidoProcedimentoMedico.PedidoProcedimentoMedicoWS;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
@Path("pedidosProcedimentosMedicos")
@Produces(MediaType.APPLICATION_JSON)
public class PedidoProcedimentoMedicoController {

    @Inject private WebserviceUtils webserviceUtils;

    private PedidoProcedimentoMedicoWS getPedidoProcedimentoMedicoWS() throws MalformedURLException {
        return (PedidoProcedimentoMedicoWS) webserviceUtils.getService("pedidoProcedimentoMedicoWS", PedidoProcedimentoMedicoWS.class);
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") long id) throws MalformedURLException {
        PedidoProcedimentoMedicoWS pedidoProcedimentoMedicoWS = getPedidoProcedimentoMedicoWS();
        return Response.ok(pedidoProcedimentoMedicoWS.get(id)).build();
    }

    @GET
    @Wrapped(element = "pedidosProcedimentosMedicos")
    public Response list() throws MalformedURLException {
        PedidoProcedimentoMedicoWS pedidoProcedimentoMedicoWS = getPedidoProcedimentoMedicoWS();
        List<PedidoProcedimentoMedico> list = pedidoProcedimentoMedicoWS.list();
        return Response.ok(new GenericEntity<List<PedidoProcedimentoMedico>>(list) {}).build();
    }
}
