package br.pucminas.prototype.controllers;

import br.pucminas.prototype.util.WebserviceUtils;
import br.pucminas.prototype.webservices.medidorBatimentoCardiaco.EquipamentoBatimentoCardiaco;
import br.pucminas.prototype.webservices.medidorBatimentoCardiaco.MedidorBatimentoCardiaco;
import br.pucminas.prototype.webservices.medidorPressaoArterial.EquipamentoPressaoArterial;
import br.pucminas.prototype.webservices.medidorPressaoArterial.MedidorPressaoArterial;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
@Path("sensoresInteligentes")
@Produces(MediaType.APPLICATION_JSON)
public class SensoresInteligentesController {

    @Inject private WebserviceUtils webserviceUtils;

    private MedidorPressaoArterial getMedidorPressaoArterial() throws MalformedURLException {
        return (MedidorPressaoArterial)
                webserviceUtils.getService("medidorPressaoArterial", MedidorPressaoArterial.class);
    }

    private MedidorBatimentoCardiaco getMedidorBatimentoCardiaco() throws MalformedURLException {
        return (MedidorBatimentoCardiaco)
                webserviceUtils.getService("medidorBatimentoCardiaco", MedidorBatimentoCardiaco.class);
    }

    @GET
    @Path("aferirPressaoArterial/{equipamentoId}")
    public Response aferirPressaoArterial(@PathParam("equipamentoId") long id) throws MalformedURLException {
        MedidorPressaoArterial medidorPressaoArterial = getMedidorPressaoArterial();
        return Response.ok(medidorPressaoArterial.aferirPressaoArterial(id)).build();
    }

    @GET
    @Path("equipamentosPressaoArterial")
    @Wrapped(element = "equipamentosPressaoArterial")
    public Response listEquipamentoPressaoArterial() throws MalformedURLException {
        MedidorPressaoArterial medidorPressaoArterial = getMedidorPressaoArterial();
        List<EquipamentoPressaoArterial> list = medidorPressaoArterial.list();
        return Response.ok(new GenericEntity<List<EquipamentoPressaoArterial>>(list) {}).build();
    }

    @GET
    @Path("equipamentosPressaoArterial/{equipamentoId}")
    public Response getEquipamentoPressaoArterial(@PathParam("equipamentoId") long id) throws MalformedURLException {
        MedidorPressaoArterial medidorPressaoArterial = getMedidorPressaoArterial();
        return Response.ok(medidorPressaoArterial.get(id)).build();
    }

    @GET
    @Path("aferirBatimentosCardiacos/{equipamentoId}")
    public Response aferirBatimentosCardiacos(@PathParam("equipamentoId") long id) throws MalformedURLException {
        MedidorBatimentoCardiaco medidorBatimentoCardiaco = getMedidorBatimentoCardiaco();
        return Response.ok(medidorBatimentoCardiaco.aferirBatimentosCardiaco(id)).build();
    }

    @GET
    @Path("equipamentosBatimentosCardiacos")
    @Wrapped(element = "equipamentoBatimentosCardiacos")
    public Response listEquipamentoBatimentosCardiacos() throws MalformedURLException {
        MedidorBatimentoCardiaco medidorBatimentoCardiaco = getMedidorBatimentoCardiaco();
        List<EquipamentoBatimentoCardiaco> list = medidorBatimentoCardiaco.list();
        return Response.ok(new GenericEntity<List<EquipamentoBatimentoCardiaco>>(list) {}).build();
    }

    @GET
    @Path("equipamentosBatimentosCardiacos/{equipamentoId}")
    public Response getEquipamentoBatimentosCardiacos(@PathParam("equipamentoId") long id) throws MalformedURLException {
        MedidorBatimentoCardiaco medidorBatimentoCardiaco = getMedidorBatimentoCardiaco();
        return Response.ok(medidorBatimentoCardiaco.get(id)).build();
    }
}
