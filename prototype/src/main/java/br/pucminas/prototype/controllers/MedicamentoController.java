package br.pucminas.prototype.controllers;

import br.pucminas.prototype.util.WebserviceUtils;
import br.pucminas.prototype.webservices.medicamento.Medicamento;
import br.pucminas.prototype.webservices.medicamento.MedicamentoWS;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
@Path("medicamentos")
@Produces(MediaType.APPLICATION_JSON)
public class MedicamentoController {

    @Inject private WebserviceUtils webserviceUtils;

    private MedicamentoWS getMedicamentoWS() throws MalformedURLException {
        return (MedicamentoWS) webserviceUtils.getService("medicamentoWS", MedicamentoWS.class);
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") long id) throws MalformedURLException {
        MedicamentoWS medicamentoWS = getMedicamentoWS();
        return Response.ok(medicamentoWS.get(id)).build();
    }

    @GET
    @Wrapped(element = "medicamentos")
    public Response list() throws MalformedURLException {
        MedicamentoWS medicamentoWS = getMedicamentoWS();
        List<Medicamento> list = medicamentoWS.list();
        return Response.ok(new GenericEntity<List<Medicamento>>(list) {}).build();
    }
}
