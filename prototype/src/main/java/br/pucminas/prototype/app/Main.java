package br.pucminas.prototype.app;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.jaxrs.JAXRSArchive;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Eric Sant'Anna
 */
public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("properties file: " + System.getProperty("swarm.propertiesFile"));
        System.getProperties().load(Files.newInputStream(Paths.get(System.getProperty("swarm.propertiesFile"))));

        Swarm swarm = new Swarm(args);

        swarm.start();

        JAXRSArchive deployment = ShrinkWrap.create(JAXRSArchive.class);
        deployment.addPackages(true, "br.pucminas.prototype");
        deployment.staticContent();
        deployment.addAllDependencies();

        swarm.deploy(deployment);
    }
}
