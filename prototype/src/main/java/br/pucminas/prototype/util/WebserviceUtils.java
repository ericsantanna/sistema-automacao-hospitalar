package br.pucminas.prototype.util;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class WebserviceUtils {

    public <T> Object getService(String serviceId, Class<T> interfaceClass) throws MalformedURLException {
        QName qNameService = new QName(getServiceNamespace(serviceId), getServiceName(serviceId));
        QName qNamePort = new QName(getServiceNamespace(serviceId),getServicePost(serviceId));
        URL url = new URL(getServiceAddress(serviceId));
        Service service = Service.create(url, qNameService);

        return service.getPort(qNamePort, interfaceClass);
    }

    public String getServiceAddress(String serviceId) {
        return System.getProperty("webservice." + serviceId + ".address");
    }

    public String getServiceNamespace(String serviceId) {
        return System.getProperty("webservice." + serviceId + ".namespace");
    }

    public String getServiceName(String serviceId) {
        return System.getProperty("webservice." + serviceId + ".name");
    }

    public String getServicePost(String serviceId) {
        return System.getProperty("webservice." + serviceId + ".port");
    }
}
