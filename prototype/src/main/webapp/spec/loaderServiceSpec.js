/**
 * Created by eric.santanna on 23/03/2017.
 */
describe('testing tests', function() {
    var LoaderService;

    beforeEach(module('WfpApp'));

    beforeEach(inject(function(_LoaderService_){
        LoaderService = _LoaderService_;
    }));

    it('too many closes', function() {
        LoaderService.open();
        LoaderService.close();
        LoaderService.close();
        LoaderService.close();
        expect(LoaderService.modalInstance).toBeFalsy();
        expect(LoaderService.queue).toEqual(0);
    });

    it('too many opens', function() {
        LoaderService.open();
        LoaderService.close();
        LoaderService.open();
        LoaderService.open();
        LoaderService.open();
        expect(LoaderService.modalInstance).toBeTruthy();
        expect(LoaderService.queue).toEqual(3);
    });
});