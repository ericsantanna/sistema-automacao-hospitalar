<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<md-content class="md-padding" layout-xs="column" layout="row">
    <div flex-gt-md="10"></div>
    <md-button flex-gt-sm="50" flex-gt-md="40" layout-padding class="md-raised md-primary" ui-sref="materiaisHospitalaresIndex">
        <span class="fa fa-stethoscope" style="font-size: 50px;"></span><br>
        Materiais hospitalares
    </md-button>
    <md-button flex-gt-sm="50" flex-gt-md="40" layout-padding class="md-raised md-primary" ui-sref="medicamentosIndex">
        <span class="fa fa-plus-square" style="font-size: 50px;"></span><br>
        Medicamentos
    </md-button>
</md-content>
