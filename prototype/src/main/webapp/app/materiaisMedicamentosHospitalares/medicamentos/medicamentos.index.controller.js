/**
 * Created by ericsantanna on 14/05/17.
 */
var medicamentosIndexController = [
    '$scope', '$state', 'Medicamento',
    function($scope, $state, Medicamento) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchMedicamentos = function () {
            $scope.medicamentos = Medicamento.query($scope.query, function success(medicamentos) {
                $scope.medicamentos = medicamentos;
            }).$promise;
        };

        $scope.fetchMedicamentos();

        $scope.abrirMedicamento = function(id) {
            $state.go('medicamentosInfo', {id: id});
        };

    }
];