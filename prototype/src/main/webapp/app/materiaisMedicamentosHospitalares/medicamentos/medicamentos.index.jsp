<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="medicamentos">
            <thead md-head md-order="query.order" md-on-reorder="fetchMedicamentos">
            <tr md-row>
                <th md-column>Nome</th>
                <th md-column>Marca</th>
                <th md-column>Quantidade</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="medicamento in medicamentos" ng-click="abrirMedicamento(medicamento.id)">
                <td md-cell>{{medicamento.nome}}</td>
                <td md-cell>{{medicamento.marca}}</td>
                <td md-cell>{{medicamento.quantidade}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{medicamentos.length}}" md-on-paginate="fetchMedicamentos" md-page-select></md-table-pagination>
</md-content>
