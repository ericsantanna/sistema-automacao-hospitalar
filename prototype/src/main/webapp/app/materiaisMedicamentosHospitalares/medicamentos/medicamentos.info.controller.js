/**
 * Created by ericsantanna on 14/05/17.
 */
var medicamentosInfoController = [
    '$scope', '$state', '$stateParams', 'Medicamento',
    function($scope, $state, $stateParams, Medicamento) {
        'use strict';

        $scope.getMedicamento = function () {
            $scope.medicamento = Medicamento.get({id: $stateParams.id}, function(medicamento) {
                $scope.medicamento = medicamento;
            }).$promise;
        };

        $scope.getMedicamento();
    }
];