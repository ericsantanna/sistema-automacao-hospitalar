<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <ng-form name="medicamentoForm">
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs="60">
                <label>Nome</label>
                <input ng-model="medicamento.nome" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs="40">
                <label>Marca</label>
                <input ng-model="medicamento.marca" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs="40">
                <label>Quantidade</label>
                <input ng-model="medicamento.quantidade" readonly>
            </md-input-container>
        </div>
    </ng-form>
</md-content>
