/**
 * Created by ericsantanna on 14/05/17.
 */
var medicamento = ['$resource', 'ResourceInterceptors', function($resource, ResourceInterceptors) {
    return $resource(root + '/api/medicamentos/:id', { id: '@id' }, ResourceInterceptors);
}];
