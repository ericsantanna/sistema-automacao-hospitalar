/**
 * Created by ericsantanna on 14/05/17.
 */
var materiaisHospitalaresIndexController = [
    '$scope', '$state', 'MaterialHospitalar',
    function($scope, $state, MaterialHospitalar) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchMateriaisHospitalares = function () {
            $scope.materiaisHospitalares = MaterialHospitalar.query($scope.query, function success(materiaisHospitalares) {
                $scope.materiaisHospitalares = materiaisHospitalares;
            }).$promise;
        };

        $scope.fetchMateriaisHospitalares();

        $scope.abrirMaterialHospitalar = function(id) {
            $state.go('materiaisHospitalaresInfo', {id: id});
        };

    }
];