/**
 * Created by ericsantanna on 14/05/17.
 */
var materiaisHospitalaresInfoController = [
    '$scope', '$state', '$stateParams', 'MaterialHospitalar',
    function($scope, $state, $stateParams, MaterialHospitalar) {
        'use strict';

        $scope.getMaterialHospitalar = function () {
            $scope.materialHospitalar = MaterialHospitalar.get({id: $stateParams.id}, function(materialHospitalar) {
                $scope.materialHospitalar = materialHospitalar;
            }).$promise;
        };

        $scope.getMaterialHospitalar();
    }
];