<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="materiaisHospitalares">
            <thead md-head md-order="query.order" md-on-reorder="fetchMateriaisHospitalares">
            <tr md-row>
                <th md-column>Nome</th>
                <th md-column>Marca</th>
                <th md-column>Quantidade</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="materialHospitalar in materiaisHospitalares" ng-click="abrirMaterialHospitalar(materialHospitalar.id)">
                <td md-cell>{{materialHospitalar.nome}}</td>
                <td md-cell>{{materialHospitalar.marca}}</td>
                <td md-cell>{{materialHospitalar.quantidade}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{materiaisHospitalares.length}}" md-on-paginate="fetchMateriaisHospitalares" md-page-select></md-table-pagination>
</md-content>
