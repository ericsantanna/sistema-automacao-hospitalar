<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <ng-form name="medidorBatimentoCardiacoForm">
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Marca</label>
                <input ng-model="medidorBatimentoCardiaco.marca" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Modelo</label>
                <input ng-model="medidorBatimentoCardiaco.modelo" readonly>
            </md-input-container>
        </div>
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Serial number</label>
                <input ng-model="medidorBatimentoCardiaco.serialNumber" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Software</label>
                <input ng-model="medidorBatimentoCardiaco.software" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Versão</label>
                <input ng-model="medidorBatimentoCardiaco.versao" readonly>
            </md-input-container>
        </div>

        <div layout-gt-sm="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Status</label>
                <input ng-model="medidorBatimentoCardiaco.status" readonly>
            </md-input-container>
        </div>
    </ng-form>
</md-content>
