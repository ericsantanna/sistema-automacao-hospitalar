<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="medidoresBatimentoCardiaco">
            <thead md-head md-order="query.order" md-on-reorder="fetchMedidoresBatimentoCardiaco">
            <tr md-row>
                <th md-column>Marca</th>
                <th md-column>Modelo</th>
                <th md-column>Serial Number</th>
                <th md-column>Software</th>
                <th md-column>Versão</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="medidorBatimentoCardiaco in medidoresBatimentoCardiaco" ng-click="abrirMedidorBatimentoCardiaco(medidorBatimentoCardiaco.id)">
                <td md-cell>{{medidorBatimentoCardiaco.marca}}</td>
                <td md-cell>{{medidorBatimentoCardiaco.modelo}}</td>
                <td md-cell>{{medidorBatimentoCardiaco.serialNumber}}</td>
                <td md-cell>{{medidorBatimentoCardiaco.software}}</td>
                <td md-cell>{{medidorBatimentoCardiaco.versao}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{medidoresBatimentoCardiaco.length}}" md-on-paginate="fetchMedidoresBatimentoCardiaco" md-page-select></md-table-pagination>
</md-content>
