/**
 * Created by ericsantanna on 14/05/17.
 */
var medidoresBatimentoCardiacoInfoController = [
    '$scope', '$state', '$stateParams', 'MedidorBatimentoCardiaco',
    function($scope, $state, $stateParams, MedidorBatimentoCardiaco) {
        'use strict';

        $scope.getMedidorBatimentoCardiaco = function () {
            $scope.medidorBatimentoCardiaco = MedidorBatimentoCardiaco.getEquipamentoBatimentosCardiacos({id: $stateParams.id}, function(medidorBatimentoCardiaco) {
                $scope.medidorBatimentoCardiaco = medidorBatimentoCardiaco;
            }).$promise;
        };

        $scope.getMedidorBatimentoCardiaco();
    }
];