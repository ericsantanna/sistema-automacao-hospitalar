/**
 * Created by ericsantanna on 14/05/17.
 */
var medidoresBatimentoCardiacoIndexController = [
    '$scope', '$state', 'MedidorBatimentoCardiaco',
    function($scope, $state, MedidorBatimentoCardiaco) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchMedidoresBatimentoCardiaco = function () {
            $scope.medidoresBatimentoCardiaco = MedidorBatimentoCardiaco.listEquipamentosBatimentosCardiacos($scope.query, function success(medidoresBatimentoCardiaco) {
                $scope.medidoresBatimentoCardiaco = medidoresBatimentoCardiaco;
            }).$promise;
        };

        $scope.fetchMedidoresBatimentoCardiaco();

        $scope.abrirMedidorBatimentoCardiaco = function(id) {
            $state.go('medidoresBatimentoCardiacoInfo', {id: id});
        };

    }
];