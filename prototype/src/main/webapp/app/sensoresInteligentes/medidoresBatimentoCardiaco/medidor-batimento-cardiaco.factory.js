/**
 * Created by ericsantanna on 14/05/17.
 */
var medidorBatimentoCardiaco = ['$resource', 'ResourceInterceptors', function($resource, ResourceInterceptors) {
    var customInterceptor = ResourceInterceptors;
    customInterceptor.listEquipamentosBatimentosCardiacos = {
        isArray: true,
        url: root + '/api/sensoresInteligentes/equipamentosBatimentosCardiacos/:id',
        params: {id: '@id'}
    };
    customInterceptor.getEquipamentoBatimentosCardiacos = {
        url: root + '/api/sensoresInteligentes/equipamentosBatimentosCardiacos/:id',
        params: {id: '@id'}
    };
    customInterceptor.aferirBatimentosCardiacos = {
        url: root + '/api/sensoresInteligentes/aferirBatimentosCardiacos/:id',
        params: {id: '@id'}
    };
    return $resource(root + '/api/sensoresInteligentes/:id', { id: '@id' }, customInterceptor);
}];
