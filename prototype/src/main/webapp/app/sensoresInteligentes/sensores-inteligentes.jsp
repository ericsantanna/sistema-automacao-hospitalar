<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<md-content class="md-padding" layout-xs="column" layout="row">
    <div flex-gt-md="10"></div>
    <md-button flex-gt-sm="50" flex-gt-md="40" layout-padding class="md-raised md-primary" ui-sref="medidoresBatimentoCardiacoIndex">
        <span class="fa fa-heartbeat" style="font-size: 50px;"></span><br>
        Medidores Batimento Cardíaco
    </md-button>
    <md-button flex-gt-sm="50" flex-gt-md="40" layout-padding class="md-raised md-primary" ui-sref="medidoresPressaoArterialIndex">
        <span class="fa fa-thermometer-three-quarters" style="font-size: 50px;"></span><br>
        Medidores Pressão Arterial
    </md-button>
</md-content>
