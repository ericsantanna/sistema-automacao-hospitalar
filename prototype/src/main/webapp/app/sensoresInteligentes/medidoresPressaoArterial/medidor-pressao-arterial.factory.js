/**
 * Created by ericsantanna on 14/05/17.
 */
var medidorPressaoArterial = ['$resource', 'ResourceInterceptors', function($resource, ResourceInterceptors) {
    var customInterceptor = ResourceInterceptors;
    customInterceptor.listEquipamentosPressaoArterial = {
        isArray: true,
        url: root + '/api/sensoresInteligentes/equipamentosPressaoArterial/:id',
        params: {id: '@id'}
    };
    customInterceptor.getEquipamentoPressaoArterial = {
        url: root + '/api/sensoresInteligentes/equipamentosPressaoArterial/:id',
        params: {id: '@id'}
    };
    customInterceptor.aferirPressaoArterial = {
        url: root + '/api/sensoresInteligentes/aferirPressaoArterial/:id',
        params: {id: '@id'}
    };
    return $resource(root + '/api/sensoresInteligentes/:id', { id: '@id' }, customInterceptor);
}];
