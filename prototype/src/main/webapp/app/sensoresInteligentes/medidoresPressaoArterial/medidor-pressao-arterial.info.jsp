<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <ng-form name="medidorPressaoArterialForm">
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Marca</label>
                <input ng-model="medidorPressaoArterial.marca" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Modelo</label>
                <input ng-model="medidorPressaoArterial.modelo" readonly>
            </md-input-container>
        </div>
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Serial number</label>
                <input ng-model="medidorPressaoArterial.serialNumber" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Software</label>
                <input ng-model="medidorPressaoArterial.software" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Versão</label>
                <input ng-model="medidorPressaoArterial.versao" readonly>
            </md-input-container>
        </div>

        <div layout-gt-sm="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Status</label>
                <input ng-model="medidorPressaoArterial.status" readonly>
            </md-input-container>
        </div>
    </ng-form>
</md-content>
