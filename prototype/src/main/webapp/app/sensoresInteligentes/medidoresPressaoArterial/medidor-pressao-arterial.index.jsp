<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="medidoresPressaoArterial">
            <thead md-head md-order="query.order" md-on-reorder="fetchMedidoresPressaoArterial">
            <tr md-row>
                <th md-column>Marca</th>
                <th md-column>Modelo</th>
                <th md-column>Serial Number</th>
                <th md-column>Software</th>
                <th md-column>Versão</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="medidorPressaoArterial in medidoresPressaoArterial" ng-click="abrirMedidorPressaoArterial(medidorPressaoArterial.id)">
                <td md-cell>{{medidorPressaoArterial.marca}}</td>
                <td md-cell>{{medidorPressaoArterial.modelo}}</td>
                <td md-cell>{{medidorPressaoArterial.serialNumber}}</td>
                <td md-cell>{{medidorPressaoArterial.software}}</td>
                <td md-cell>{{medidorPressaoArterial.versao}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{medidoresPressaoArterial.length}}" md-on-paginate="fetchMedidoresPressaoArterial" md-page-select></md-table-pagination>
</md-content>
