/**
 * Created by ericsantanna on 14/05/17.
 */
var medidoresPressaoArterialIndexController = [
    '$scope', '$state', 'MedidorPressaoArterial',
    function($scope, $state, MedidorPressaoArterial) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchMedidoresPressaoArterial = function () {
            $scope.medidoresPressaoArterial = MedidorPressaoArterial.listEquipamentosPressaoArterial($scope.query, function success(medidoresPressaoArterial) {
                $scope.medidoresPressaoArterial = medidoresPressaoArterial;
            }).$promise;
        };

        $scope.fetchMedidoresPressaoArterial();

        $scope.abrirMedidorPressaoArterial = function(id) {
            $state.go('medidoresPressaoArterialInfo', {id: id});
        };

    }
];