/**
 * Created by ericsantanna on 14/05/17.
 */
var medidoresPressaoArterialInfoController = [
    '$scope', '$state', '$stateParams', 'MedidorPressaoArterial',
    function($scope, $state, $stateParams, MedidorPressaoArterial) {
        'use strict';

        $scope.getMedidorPressaoArterial = function () {
            $scope.medidorPressaoArterial = MedidorPressaoArterial.getEquipamentoPressaoArterial({id: $stateParams.id}, function(medidorPressaoArterial) {
                $scope.medidorPressaoArterial = medidorPressaoArterial;
            }).$promise;
        };

        $scope.getMedidorPressaoArterial();
    }
];