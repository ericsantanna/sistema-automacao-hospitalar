<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="SistemaAutomacaoHospitalarApp">
<head>
	<title>PUC Minas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
	<link rel="stylesheet" href="../assets/libs/css/angular-material.min.css">
	<link rel="stylesheet" href="../assets/libs/css/font-awesome.min.css">
	<link rel="stylesheet" href="../assets/libs/css/md-data-table.min.css">
	<link rel="stylesheet" href="../assets/css/styles.css">
	
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/moment-with-locales.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/jquery.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-resource.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-messages.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-sanitize.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-locale_pt-br.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-animate.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-aria.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-material.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/md-data-table.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/Chart.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-chart.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-input-masks-dependencies.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-input-masks.br.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-ui-router.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-breadcrumb.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-br-filters.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/shared/page/header.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/home/home.js"></script>

	<%--Factories--%>
	<script type="text/javascript" charset="UTF-8" src="../app/sensoresInteligentes/medidoresBatimentoCardiaco/medidor-batimento-cardiaco.factory.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/sensoresInteligentes/medidoresPressaoArterial/medidor-pressao-arterial.factory.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/recursosHotelariaHospitalar/recursos-hotelaria-hospitalar.factory.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/pedidosProcedimentosMedicos/pedido-procedimento-medico.factory.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/materiaisMedicamentosHospitalares/materiaisHospitalares/materiais-hospitalares.factory.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/materiaisMedicamentosHospitalares/medicamentos/medicamentos.factory.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/laboratorioExames/hemograma/hemograma.factory.js"></script>

	<%--Controllers--%>
	<script type="text/javascript" charset="UTF-8" src="../app/sensoresInteligentes/medidoresBatimentoCardiaco/medidor-batimento-cardiaco.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/sensoresInteligentes/medidoresBatimentoCardiaco/medidor-batimento-cardiaco.info.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/sensoresInteligentes/medidoresPressaoArterial/medidor-pressao-arterial.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/sensoresInteligentes/medidoresPressaoArterial/medidor-pressao-arterial.info.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/recursosHotelariaHospitalar/recursos-hotelaria-hospitalar.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/recursosHotelariaHospitalar/recursos-hotelaria-hospitalar.info.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/pedidosProcedimentosMedicos/pedido-procedimento-medico.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/pedidosProcedimentosMedicos/pedido-procedimento-medico.info.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/materiaisMedicamentosHospitalares/materiaisHospitalares/materiais-hospitalares.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/materiaisMedicamentosHospitalares/materiaisHospitalares/materiais-hospitalares.info.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/materiaisMedicamentosHospitalares/medicamentos/medicamentos.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/materiaisMedicamentosHospitalares/medicamentos/medicamentos.info.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/laboratorioExames/hemograma/hemograma.index.controller.js"></script>
	<script type="text/javascript" charset="UTF-8" src="../app/laboratorioExames/hemograma/hemograma.info.controller.js"></script>

	<%--App--%>
	<script type="text/javascript" charset="UTF-8" src="../app/app.js"></script>

</head>
<body>
	<jsp:include page="shared/page/header.jsp" />
	<div layout="column" class="relative" layout-fill role="main">
        <div id="views" ui-view>
        </div>
    </div>
</body>
</html>