root = window.location.href.toString().split('/', 3).join('/');

moment.locale('pt-br');

urlParams = {};

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

// Usado porque o angular resource trata todo resultado como objeto
var textPlainInterceptor = {
    response: function(response) {
        if(response.headers()['content-type'].indexOf('text/plain') > -1) {
            return response.data;
        }
        return response;
    }
};

angular
.module('SistemaAutomacaoHospitalarApp', ['ngResource', 'ui.router', 'ngAria', 'ngAnimate', 'ngMaterial', 'md.data.table', 'chart.js', 'ngSanitize', 'ngMessages', 'ui.utils.masks', 'idf.br-filters', 'ncy-angular-breadcrumb'])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
	    .state('home', {
	    	url: '/home',
	    	templateUrl: 'home/home.jsp',
	    	controller: 'HomeController',
	    	ncyBreadcrumb: {
	    		label: 'Home'
	    	}
	    })

        /* Sensores inteligentes */
        .state('sensoresInteligentesIndex', {
            url: '/sensoresInteligentes',
            templateUrl: 'sensoresInteligentes/sensores-inteligentes.jsp',
            ncyBreadcrumb: {
                label: 'Sensores inteligentes'
            }
        })
        .state('medidoresPressaoArterialIndex', {
            url: '/medidoresPressaoArterial',
            templateUrl: 'sensoresInteligentes/medidoresPressaoArterial/medidor-pressao-arterial.index.jsp',
            controller: 'MedidoresPressaoArterialIndexController',
            ncyBreadcrumb: {
                label: 'Medidores Pressão Arterial',
                parent: 'sensoresInteligentesIndex'
            }
        })
        .state('medidoresPressaoArterialInfo', {
            url: '/medidoresPressaoArterial/{id}',
            templateUrl: 'sensoresInteligentes/medidoresPressaoArterial/medidor-pressao-arterial.info.jsp',
            controller: 'MedidoresPressaoArterialInfoController',
            ncyBreadcrumb: {
                label: 'Status',
                parent: 'medidoresPressaoArterialIndex'
            }
        })
        .state('medidoresBatimentoCardiacoIndex', {
            url: '/medidoresBatimentoCardiaco',
            templateUrl: 'sensoresInteligentes/medidoresBatimentoCardiaco/medidor-batimento-cardiaco.index.jsp',
            controller: 'MedidoresBatimentoCardiacoIndexController',
            ncyBreadcrumb: {
                label: 'Medidores Batimentos Cardíacos',
                parent: 'sensoresInteligentesIndex'
            }
        })
        .state('medidoresBatimentoCardiacoInfo', {
            url: '/medidoresBatimentoCardiaco/{id}',
            templateUrl: 'sensoresInteligentes/medidoresBatimentoCardiaco/medidor-batimento-cardiaco.info.jsp',
            controller: 'MedidoresBatimentoCardiacoInfoController',
            ncyBreadcrumb: {
                label: 'Status',
                parent: 'medidoresBatimentoCardiacoIndex'
            }
        })

        /* Hotelaria hospitalar */
        .state('recursosHotelariaHospilatarIndex', {
            url: '/recursosHotelariaHospilatar',
            templateUrl: 'recursosHotelariaHospitalar/recursos-hotelaria-hospitalar.index.jsp',
            controller: 'RecursosHotelariaHospitalarIndexController',
            ncyBreadcrumb: {
                label: 'Recursos de Hotelaria Hospitalar'
            }
        })
        .state('recursosHotelariaHospilatarInfo', {
            url: '/recursosHotelariaHospilatar/{id}',
            templateUrl: 'recursosHotelariaHospitalar/recursos-hotelaria-hospitalar.info.jsp',
            controller: 'RecursosHotelariaHospitalarInfoController',
            ncyBreadcrumb: {
                label: 'Detalhes',
                parent: 'recursosHotelariaHospilatarIndex'
            }
        })

        /* Controle Financeiro */
        .state('pedidosProcedimentosMedicosIndex', {
            url: '/pedidosProcedimentosMedicos',
            templateUrl: 'pedidosProcedimentosMedicos/pedido-procedimento-medico.index.jsp',
            controller: 'pedidosProcedimentosMedicosIndexController',
            ncyBreadcrumb: {
                label: 'Pedidos de procedimentos médicos'
            }
        })
        .state('pedidosProcedimentosMedicosInfo', {
            url: '/pedidosProcedimentosMedicos/{id}',
            templateUrl: 'pedidosProcedimentosMedicos/pedido-procedimento-medico.info.jsp',
            controller: 'pedidosProcedimentosMedicosInfoController',
            ncyBreadcrumb: {
                label: 'Detalhes',
                parent: 'pedidosProcedimentosMedicosIndex'
            }
        })

        /* Home materiais e medicamentos hospitalares */
        .state('materiaisMedicamentosHospitalaresIndex', {
            url: '/materiaisMedicamentosHospitalares',
            templateUrl: 'materiaisMedicamentosHospitalares/materiais-medicamentos-hospitalares.jsp',
            ncyBreadcrumb: {
                label: 'Materiais e medicamentos hospitalares'
            }
        })

        /* Materiais hospitalares */
        .state('materiaisHospitalaresIndex', {
            url: '/materiaisHospitalares',
            templateUrl: 'materiaisMedicamentosHospitalares/materiaisHospitalares/materiais-hospitalares.index.jsp',
            controller: 'materiaisHospitalaresIndexController',
            ncyBreadcrumb: {
                label: 'Materiais',
                parent: 'materiaisMedicamentosHospitalaresIndex'
            }
        })
        .state('materiaisHospitalaresInfo', {
            url: '/materiaisHospitalares/{id}',
            templateUrl: 'materiaisMedicamentosHospitalares/materiaisHospitalares/materiais-hospitalares.info.jsp',
            controller: 'materiaisHospitalaresInfoController',
            ncyBreadcrumb: {
                label: 'Detalhes',
                parent: 'materiaisHospitalaresIndex'
            }
        })

        /* Medicamentos */
        .state('medicamentosIndex', {
            url: '/medicamentos',
            templateUrl: 'materiaisMedicamentosHospitalares/medicamentos/medicamentos.index.jsp',
            controller: 'medicamentosIndexController',
            ncyBreadcrumb: {
                label: 'Medicamentos',
                parent: 'materiaisMedicamentosHospitalaresIndex'
            }
        })
        .state('medicamentosInfo', {
            url: '/medicamentos/{id}',
            templateUrl: 'materiaisMedicamentosHospitalares/medicamentos/medicamentos.info.jsp',
            controller: 'medicamentosInfoController',
            ncyBreadcrumb: {
                label: 'Detalhes',
                parent: 'medicamentosIndex'
            }
        })

        /* Home laboratorio de exames */
        .state('laboratorioExamesIndex', {
            url: '/laboratorioExames',
            templateUrl: 'laboratorioExames/laboratorio-exames.jsp',
            ncyBreadcrumb: {
                label: 'Laboratório de exames'
            }
        })

        /* Hemograma */
        .state('hemogramasIndex', {
            url: '/hemogramas',
            templateUrl: 'laboratorioExames/hemograma/hemograma.index.jsp',
            controller: 'hemogramasIndexController',
            ncyBreadcrumb: {
                label: 'Hemogramas',
                parent: 'laboratorioExamesIndex'
            }
        })
        .state('hemogramasInfo', {
            url: '/hemogramas/{id}',
            templateUrl: 'laboratorioExames/hemograma/hemograma.info.jsp',
            controller: 'hemogramasInfoController',
            ncyBreadcrumb: {
                label: 'Detalhes',
                parent: 'hemogramasIndex'
            }
        });

    $urlRouterProvider.otherwise('/home');
}])
.config(function($breadcrumbProvider) {
	$breadcrumbProvider.setOptions({
		prefixStateName: 'home',
		template: [
        '<div>',
        '    <span ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">',
        '        <span ng-switch-when="false">',
        '            <md-button ui-sref="{{step.name}}">',
        '                {{step.ncyBreadcrumbLabel}}',
        '            </md-button>',
        '        </span>',
        '        <span ng-switch-when="false">></span>',
        '        <span ng-switch-when="true">',
        '            <md-button>',
        '                {{step.ncyBreadcrumbLabel}}',
        '            </md-button>',
        '        </span>',
        '    </span>',
        '</div>'].join('')
	});
})
.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('cyan',{'default': '900'})
        .accentPalette('blue',{'default': '500'})
        .backgroundPalette('grey',{'default': '300'});
    // $mdThemingProvider.theme('altTheme')
    //     .primaryPalette('grey',{'default': '900'})
    //     .accentPalette('grey',{'default': '700'})
    //     .dark();
    // $mdThemingProvider.setDefaultTheme('default');
    // $mdThemingProvider.alwaysWatchTheme(true);
})
.config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
        return moment(date).format('DD/MM/YYYY HH:mm:ss');
    };
})
.filter("asDate", function(){
    return function(input) {
        return input ? Date.parse(input) : '';
    };
})
.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push(['$q', '$window', '$timeout', '$rootScope', '$injector', function($q, $window, $timeout, $rootScope, $injector) {
        return {
	        'request': function(response) {
	        	response.headers['xhr'] = true; // Informa ao servidor que é uma requisição ajax
	        	return response;
	        },
	        'response': function(response) {
	            return response;
	        },
	        'requestError': function(rejection) {
                console.error('requestError', rejection);
            	if(rejection.headers()['content-type'] && rejection.headers()['content-type'].indexOf('text/plain') !== -1) {
                    var $mdToast = $injector.get("$mdToast");
            		$mdToast.error(rejection.data);
	        	}
	        	return $q.reject(rejection);
	        },
	        'responseError': function(rejection) {
                console.error('responseError', rejection);
                var $mdToast = $injector.get("$mdToast");
	        	if(rejection.headers()['content-type'] && rejection.headers()['content-type'].indexOf('text/plain') !== -1) {
	        		$mdToast.error(rejection.data);
	        	}
                if(!rejection.status) {
                    $mdToast.error('Ops! Parece que você não está conectado à internet...');
                }
	        	if(rejection.status === 302) {
                    $injector.get('$state').go(rejection.data);
				}
	        	return $q.reject(rejection);
	        }
    	};
    }]);
}])
.filter('titleCase', function() {
    return function(input) {
        input = input || '';
        return input
            .replace(/_/g, ' ')
            .replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };
})
.directive('titleCase', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {

                var transformedInput =
                    inputValue
                    .replace(/_/g, ' ')
                    .replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});

                if (transformedInput !== inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
})
.filter("format", function () {
	return function (input) {
		var args = arguments;
		return input.replace(/\{(\d+)\}/g, function (match, capture) {
			return args[1*capture + 1];
		});
	};
})
.factory('ResourceInterceptors', function() {
	return {
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		},
        remove: {
            method: 'DELETE',
            isArray: false,
            interceptor: {
                response: function(response) {
                    if(response.headers()['content-type'].indexOf('text/plain') > -1) {
                        return response.data;
                    }
                    return response;
                }
            }
        },
		save: {
			method: 'POST',
			isArray: false,
            interceptor: {
                response: function(response) {
                	if(response.headers()['content-type'].indexOf('text/plain') > -1) {
                		return response.data;
                	}
                    return response;
                }
            }
		}
	};
})
.factory('MedidorBatimentoCardiaco', medidorBatimentoCardiaco)
.factory('MedidorPressaoArterial', medidorPressaoArterial)
.factory('RecursoHotelariaHospitalar', recursoHotelariaHospilatar)
.factory('PedidoProcedimentoMedico', pedidoProcedimentoMedico)
.factory('MaterialHospitalar', materialHospitalar)
.factory('Medicamento', medicamento)
.factory('Hemograma', hemograma)
.controller('HeaderController', headerController)
.controller('HomeController', homeController)
.controller('MedidoresPressaoArterialIndexController', medidoresPressaoArterialIndexController)
.controller('MedidoresPressaoArterialInfoController', medidoresPressaoArterialInfoController)
.controller('MedidoresBatimentoCardiacoIndexController', medidoresBatimentoCardiacoIndexController)
.controller('MedidoresBatimentoCardiacoInfoController', medidoresBatimentoCardiacoInfoController)
.controller('RecursosHotelariaHospitalarIndexController', recursosHotelariaHospitalarIndexController)
.controller('RecursosHotelariaHospitalarInfoController', recursosHotelariaHospitalarInfoController)
.controller('pedidosProcedimentosMedicosIndexController', pedidosProcedimentosMedicosIndexController)
.controller('pedidosProcedimentosMedicosInfoController', pedidosProcedimentosMedicosInfoController)
.controller('materiaisHospitalaresIndexController', materiaisHospitalaresIndexController)
.controller('materiaisHospitalaresInfoController', materiaisHospitalaresInfoController)
.controller('medicamentosIndexController', medicamentosIndexController)
.controller('medicamentosInfoController', medicamentosInfoController)
.controller('hemogramasIndexController', hemogramasIndexController)
.controller('hemogramasInfoController', hemogramasInfoController);
