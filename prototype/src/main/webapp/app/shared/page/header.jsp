<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div layout="column">
    <md-toolbar layout-align="center start">
        <md-button aria-label="Home" ui-sref="home">
            Sistema de Automação Hospitalar
        </md-button>
        <span class="pull-left" ncy-breadcrumb style="margin-bottom: 0;"></span>
    </md-toolbar>
</div>