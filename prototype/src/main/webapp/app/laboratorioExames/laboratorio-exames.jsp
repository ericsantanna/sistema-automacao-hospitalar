<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<md-content class="md-padding" layout-xs="column" layout="row">
    <div flex-gt-sm="33"></div>
    <md-button flex-gt-sm="33" layout-padding class="md-raised md-primary" ui-sref="hemogramasIndex">
        <span class="fa fa-tint" style="font-size: 50px;"></span><br>
        Hemogramas
    </md-button>
</md-content>
