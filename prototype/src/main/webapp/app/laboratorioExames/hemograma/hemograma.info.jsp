<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <ng-form name="hemogramaForm">
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs="20">
                <label>ID</label>
                <input ng-model="hemograma.id" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs="60">
                <label>Paciente</label>
                <input ng-model="hemograma.paciente.nome" readonly>
            </md-input-container>
        </div>
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs="33">
                <label>Status</label>
                <input ng-model="hemograma.status" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs="33">
                <label>Data Pedido</label>
                <md-datepicker ng-model="hemograma.dataPedido" disabled></md-datepicker>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs="33">
                <label>Data Entrega</label>
                <md-datepicker ng-model="hemograma.dataEntrega" disabled></md-datepicker>
            </md-input-container>
        </div>
        <div layout="row" layout-padding ng-if="hemograma.eritograma">
            <div flex="33">
                <md-table-container>
                    <table md-table>
                        <thead md-head>
                            <tr md-row style="text-align: left">
                                <th md-cell colspan="2">Eritograma</th>
                                <th md-cell>Valor</th>
                                <th md-cell>Unidade</th>
                            </tr>
                        </thead>
                        <tbody md-body>
                            <tr md-row>
                                <td md-cell colspan="2">Eritrócitos</td>
                                <td md-cell>{{hemograma.eritograma.eritocito.valor | number}}</td>
                                <td md-cell>{{hemograma.eritograma.eritocito.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Hemoglobina</td>
                                <td md-cell>{{hemograma.eritograma.hemoglobina.valor | number:2}}</td>
                                <td md-cell>{{hemograma.eritograma.hemoglobina.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Hematócrito</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.valor | number:2}}</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>VCM</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.vcm.valor | number:2}}</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.vcm.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>HCM</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.hcm.valor | number:2}}</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.hcm.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>CHCM</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.chcm.valor | number:2}}</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.chcm.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>RDW</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.rdw.valor | number:2}}</td>
                                <td md-cell>{{hemograma.eritograma.hematocrito.rdw.unidade}}</td>
                            </tr>
                        </tbody>
                    </table>
                </md-table-container>
            </div>
            <div flex="33">
                <md-table-container>
                    <table md-table>
                        <thead md-head>
                        <tr md-row style="text-align: left">
                            <th md-cell colspan="2">Leucograma</th>
                            <th md-cell>Valor</th>
                            <th md-cell>Unidade</th>
                        </tr>
                        </thead>
                        <tbody md-body>
                            <tr md-row>
                                <td md-cell colspan="2">Leucócitos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Neutrófilos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>Mielócitos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.mielocito.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.mielocito.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>Metamielócitos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.metamielocito.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.metamielocito.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell style="min-width: 2em"></td>
                                <td md-cell>Segmentados</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.segmentado.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.neutrofilo.segmentado.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Linfócitos Típicos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.linfocitoTipico.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.linfocitoTipico.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Monócitos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.monocito.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.monocito.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Eosinófilos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.eosinofilo.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.eosinofilo.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Basófilos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.basofilo.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.basofilo.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Bastonetes</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.bastonete.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.bastonete.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Linfócitos Atípicos</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.linfocitoAtipico.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.linfocitoAtipico.unidade}}</td>
                            </tr>
                            <tr md-row>
                                <td md-cell colspan="2">Outros</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.outro.valor | number:2}}</td>
                                <td md-cell>{{hemograma.leucograma.leucocito.outro.unidade}}</td>
                            </tr>
                        </tbody>
                    </table>
                </md-table-container>
            </div>
            <div flex="33">
                <md-table-container>
                    <table md-table>
                        <thead md-head>
                        <tr md-row style="text-align: left">
                            <th md-cell colspan="2">Plaquetas</th>
                            <th md-cell>Valor</th>
                            <th md-cell>Unidade</th>
                        </tr>
                        </thead>
                        <tbody md-body>
                            <tr md-row>
                                <td md-cell colspan="2">Plaquetas</td>
                                <td md-cell>{{hemograma.plaqueta.valor | number:2}}</td>
                                <td md-cell>{{hemograma.plaqueta.unidade}}</td>
                            </tr>
                        </tbody>
                    </table>
                </md-table-container>
            </div>
        </div>
    </ng-form>
</md-content>
