<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="hemogramas">
            <thead md-head md-order="query.order" md-on-reorder="fetchHemogramas">
            <tr md-row>
                <th md-column>ID</th>
                <th md-column>Paciente</th>
                <th md-column>Data pedido</th>
                <th md-column>Data entrega</th>
                <th md-column>Status</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="hemograma in hemogramas" ng-click="abrirHemograma(hemograma.id)">
                <td md-cell>{{hemograma.id}}</td>
                <td md-cell>{{hemograma.paciente.nome}}</td>
                <td md-cell>{{hemograma.dataPedido | asDate | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                <td md-cell>{{hemograma.dataEntrega | asDate | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                <td md-cell>{{hemograma.status | titleCase}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{hemogramas.length}}" md-on-paginate="fetchHemogramas" md-page-select></md-table-pagination>
</md-content>
