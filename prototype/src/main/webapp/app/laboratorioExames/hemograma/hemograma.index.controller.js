/**
 * Created by ericsantanna on 14/05/17.
 */
var hemogramasIndexController = [
    '$scope', '$state', 'Hemograma',
    function($scope, $state, Hemograma) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchHemogramas = function () {
            $scope.hemogramas = Hemograma.query($scope.query, function success(hemogramas) {
                $scope.hemogramas = hemogramas;
            }).$promise;
        };

        $scope.fetchHemogramas();

        $scope.abrirHemograma = function(id) {
            $state.go('hemogramasInfo', {id: id});
        };

    }
];