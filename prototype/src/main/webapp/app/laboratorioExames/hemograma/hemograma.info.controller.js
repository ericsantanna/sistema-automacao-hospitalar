/**
 * Created by ericsantanna on 14/05/17.
 */
var hemogramasInfoController = [
    '$scope', '$state', '$stateParams', 'Hemograma',
    function($scope, $state, $stateParams, Hemograma) {
        'use strict';

        $scope.getHemograma = function () {
            $scope.hemograma = Hemograma.get({id: $stateParams.id}, function(hemograma) {
                $scope.hemograma = hemograma;
            }).$promise;
        };

        $scope.getHemograma();
    }
];