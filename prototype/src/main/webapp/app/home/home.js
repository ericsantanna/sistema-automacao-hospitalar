var homeController = ['$scope', '$rootScope', '$http', '$window', function($scope, $rootScope, $http, $window) {
	$scope.logout = function() {
		$http.post(root + '/api/usuarios/logout')
		.then(function() {
			$rootScope.usuario = null;
			$window.location.href = root + '/login.jsp';
		});
	};

	$scope.go = function() {
		$window.location.href = root + '/login.jsp';
	};
}];