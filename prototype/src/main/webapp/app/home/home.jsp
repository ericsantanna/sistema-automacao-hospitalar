<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<md-content class="md-padding" layout-xs="column" layout="row" layout-padding layout-align="space-around center">
    <md-button flex-gt-sm="33" layout-padding class="md-raised md-primary" ui-sref="sensoresInteligentesIndex">
        <span class="fa fa-thermometer-1" style="font-size: 50px;"></span><br>
        Sensores inteligentes
    </md-button>
    <md-button flex-gt-sm="33" layout-padding class="md-raised md-primary" ui-sref="laboratorioExamesIndex">
        <span class="fa fa-eyedropper" style="font-size: 50px;"></span><br>
        Laboratório de exames
    </md-button>
    <md-button flex-gt-sm="33" layout-padding class="md-raised md-primary" ui-sref="pedidosProcedimentosMedicosIndex">
        <span class="fa fa-money" style="font-size: 50px;"></span><br>
        Controle financeiro
    </md-button>
</md-content>
<md-content class="md-padding" layout-xs="column" layout="row" layout-padding layout-align="space-around center">
    <md-button flex-gt-sm="33" layout-padding class="md-raised md-primary" ui-sref="recursosHotelariaHospilatarIndex">
        <span class="fa fa-hospital-o" style="font-size: 50px;"></span><br>
        Controle de hotelaria hospitalar
    </md-button>
    <md-button flex-gt-sm="33" layout-padding class="md-raised md-primary" ui-sref="materiaisMedicamentosHospitalaresIndex">
        <span class="fa fa-medkit" style="font-size: 50px;"></span><br>
        Controle de materiais e medicamentos
    </md-button>
</md-content>
