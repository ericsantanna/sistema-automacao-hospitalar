<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="pedidosProcedimentosMedicos">
            <thead md-head md-order="query.order" md-on-reorder="fetchPedidosProcedimentosMedicos">
            <tr md-row>
                <th md-column>Paciente</th>
                <th md-column>Plano de saúde</th>
                <th md-column>Valor</th>
                <th md-column>Status</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="pedidoProcedimentomedico in pedidosProcedimentosMedicos" ng-click="abrirPedidoProcedimentoMedico(pedidoProcedimentomedico.id)">
                <td md-cell>{{pedidoProcedimentomedico.paciente.nome}}</td>
                <td md-cell>{{pedidoProcedimentomedico.planoSaude.nome}}</td>
                <td md-cell>{{pedidoProcedimentomedico.valor | currency}}</td>
                <td md-cell>{{pedidoProcedimentomedico.status | titleCase}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{pedidosProcedimentosMedicos.length}}" md-on-paginate="fetchPedidosProcedimentosMedicos" md-page-select></md-table-pagination>
</md-content>
