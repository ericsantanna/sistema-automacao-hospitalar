/**
 * Created by ericsantanna on 14/05/17.
 */
var pedidosProcedimentosMedicosInfoController = [
    '$scope', '$state', '$stateParams', 'PedidoProcedimentoMedico',
    function($scope, $state, $stateParams, PedidoProcedimentoMedico) {
        'use strict';

        $scope.getPedidoProcedimentoMedico = function () {
            $scope.pedidoProcedimentomedico = PedidoProcedimentoMedico.get({id: $stateParams.id}, function(pedidoProcedimentomedico) {
                $scope.pedidoProcedimentomedico = pedidoProcedimentomedico;
            }).$promise;
        };

        $scope.getPedidoProcedimentoMedico();
    }
];