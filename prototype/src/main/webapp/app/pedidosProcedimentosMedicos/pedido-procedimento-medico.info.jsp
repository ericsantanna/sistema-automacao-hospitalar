<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <ng-form name="pedidoProcedimentomedicoForm">
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs="60">
                <label>Paciente</label>
                <input ng-model="pedidoProcedimentomedico.paciente.nome" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs="40">
                <label>Plano de saúde</label>
                <input ng-model="pedidoProcedimentomedico.planoSaude.nome" readonly>
            </md-input-container>
        </div>
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Valor</label>
                <input ng-model="pedidoProcedimentomedico.valor" ui-money-mask readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Status</label>
                <input ng-model="pedidoProcedimentomedico.status" title-case readonly>
            </md-input-container>
        </div>
    </ng-form>
</md-content>
