/**
 * Created by ericsantanna on 14/05/17.
 */
var pedidosProcedimentosMedicosIndexController = [
    '$scope', '$state', 'PedidoProcedimentoMedico',
    function($scope, $state, PedidoProcedimentoMedico) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchPedidosProcedimentosMedicos = function () {
            $scope.pedidosProcedimentosMedicos = PedidoProcedimentoMedico.query($scope.query, function success(pedidosProcedimentosMedicos) {
                $scope.pedidosProcedimentosMedicos = pedidosProcedimentosMedicos;
            }).$promise;
        };

        $scope.fetchPedidosProcedimentosMedicos();

        $scope.abrirPedidoProcedimentoMedico = function(id) {
            $state.go('pedidosProcedimentosMedicosInfo', {id: id});
        };

    }
];