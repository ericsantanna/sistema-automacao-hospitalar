/**
 * Created by ericsantanna on 14/05/17.
 */
var pedidoProcedimentoMedico = ['$resource', 'ResourceInterceptors', function($resource, ResourceInterceptors) {
    return $resource(root + '/api/pedidosProcedimentosMedicos/:id', { id: '@id' }, ResourceInterceptors);
}];
