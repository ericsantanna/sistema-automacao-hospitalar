<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <ng-form name="recursoForm">
        <div layout-gt-xs="row">
            <md-input-container class="md-block" flex-gt-xs>
                <label>Nome</label>
                <input ng-model="recurso.nome" readonly>
            </md-input-container>
            <md-input-container class="md-block" flex-gt-xs>
                <label>Quantidade</label>
                <input ng-model="recurso.quantidade" readonly>
            </md-input-container>
        </div>
    </ng-form>
</md-content>
