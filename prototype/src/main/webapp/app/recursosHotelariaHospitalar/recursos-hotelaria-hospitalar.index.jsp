<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<md-content layout-padding>
    <md-table-container>
        <table md-table md-progress="recursos">
            <thead md-head md-order="query.order" md-on-reorder="fetchRecursos">
            <tr md-row>
                <th md-column>Nome</th>
                <th md-column>Quantidade</th>
            </tr>
            </thead>
            <tbody md-body>
            <tr md-row ng-repeat="recurso in recursos" ng-click="abrirRecurso(recurso.id)">
                <td md-cell>{{recurso.nome}}</td>
                <td md-cell>{{recurso.quantidade}}</td>
            </tr>
            </tbody>
        </table>
    </md-table-container>

    <md-table-pagination md-limit="query.limit" md-limit-options="[5, 10, 15]" md-page="query.page" md-total="{{recursos.length}}" md-on-paginate="fetchRecursos" md-page-select></md-table-pagination>
</md-content>
