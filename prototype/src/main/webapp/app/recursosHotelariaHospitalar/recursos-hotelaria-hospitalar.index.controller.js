/**
 * Created by ericsantanna on 14/05/17.
 */
var recursosHotelariaHospitalarIndexController = [
    '$scope', '$state', 'RecursoHotelariaHospitalar',
    function($scope, $state, RecursoHotelariaHospitalar) {
        'use strict';

        $scope.query = {
            order: 'id',
            limit: 10,
            page: 1
        };

        $scope.fetchRecursos = function () {
            $scope.recursos = RecursoHotelariaHospitalar.query($scope.query, function success(recursos) {
                $scope.recursos = recursos;
            }).$promise;
        };

        $scope.fetchRecursos();

        $scope.abrirRecurso = function(id) {
            $state.go('recursosHotelariaHospilatarInfo', {id: id});
        };

    }
];