/**
 * Created by ericsantanna on 14/05/17.
 */
var recursosHotelariaHospitalarInfoController = [
    '$scope', '$state', '$stateParams', 'RecursoHotelariaHospitalar',
    function($scope, $state, $stateParams, RecursoHotelariaHospitalar) {
        'use strict';

        $scope.getRecurso = function () {
            $scope.recurso = RecursoHotelariaHospitalar.get({id: $stateParams.id}, function(recurso) {
                $scope.recurso = recurso;
            }).$promise;
        };

        $scope.getRecurso();
    }
];