/**
 * Created by ericsantanna on 14/05/17.
 */
var recursoHotelariaHospilatar = ['$resource', 'ResourceInterceptors', function($resource, ResourceInterceptors) {
    return $resource(root + '/api/recursosHotelariaHospilatar/:id', { id: '@id' }, ResourceInterceptors);
}];
