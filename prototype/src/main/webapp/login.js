root = window.location.href.toString().split('/', 4).join('/');
angular
.module('WfpApp', ['angular-mdToast', 'ngSanitize', 'ngMessages', 'ui.bootstrap'])
.config(['mdToastProvider', '$httpProvider', function(mdToastProvider) {
	mdToastProvider.globalTimeToLive(8000);
}])
.controller('LoginController', ['$scope', '$rootScope', '$http', '$window', 'mdToast', function($scope, $rootScope, $http, $window, mdToast) {
	$scope.usuario = {};
	$scope.modo = 'login';
	$scope.strength = 0;
	$scope.entrar = function(usuario) {
		if($scope.formLogin.$valid) {
			$http.post(root + '/api/auth/login', usuario)
				.then(function(response) {
					if(response.data.resetSenha) {
						$scope.usuario.senha = '';
						$scope.modo = 'changePassword';
					} else {
						$window.location.href = root + '/app/#/home';
					}
				}, function(response) {
					mdToast.error(response.data);
				});
		} else {
			mdToast.error('Insira um nome usuário e senha válidos');
		}
	};
	$scope.esqueciASenha = function() {
		$scope.usuario.senha = '';
		$scope.modo = 'forgotPassword';
	};

	$scope.alterarSenha = function() {
		if($scope.formLogin.$valid) {
			$http.post(root + '/api/usuarios/' + window.encodeURIComponent($scope.usuario.login) + '/changePassword', $scope.usuario)
				.then(function (response) {
					mdToast.info(response.data);
					$window.location.href = root + '/app/#/home';
				}, function (response) {
					mdToast.error(response.data);
				});
		} else {
			mdToast.error('Insira um login válido');
		}
	};

	$scope.cancelar = function() {
		$scope.modo = 'login';
	};

	$scope.enviarNovaSenha = function() {
		if($scope.formLogin.$valid) {
			$http.post(root + '/api/usuarios/' + window.encodeURIComponent($scope.usuario.login) + '/forgotPassword')
				.then(function (response) {
					mdToast.info(response.data);
					$scope.modo = 'login';
				}, function (response) {
					mdToast.error(response.data);
				});
		} else {
			mdToast.error('Insira um login válido');
		}
	};

	$scope.complexityAlert = function(strength) {
		if(strength > 0 && strength < 230) {
			return 'danger';
		}
		if(strength >= 230 && strength < 300) {
			return 'info';
		}
		if(strength >= 300) {
			return 'success';
		}
		return 'warning';
	};

	$scope.evaluatePassword = function(password) {
		var calculate = function(weight, repetitions) {
			var score = repetitions > 1 ? weight - (weight * repetitions / 10) : weight * 2.5;
			return score > 0 ? score : 0;
		};

		$scope.strength = 0;
		var number = 8,
			lower = 10,
			upper = 12,
			special = 20,
			temp = '',
			repetitions = 0;

		if(password) {
			password.split('').forEach(function(c) {
				temp += c;
				repetitions = temp.split(c).length - 1;
				if(c.match(/[0-9]/)) {
					$scope.strength += calculate(number, repetitions);
				}
				else if(c.match(/[a-z]/)) {
					$scope.strength += calculate(lower, repetitions);
				}
				else if(c.match(/[A-Z]/)) {
					$scope.strength += calculate(upper, repetitions);
				}
				else {
					$scope.strength += calculate(special, repetitions);
				}
			});
		}
	};
}]);