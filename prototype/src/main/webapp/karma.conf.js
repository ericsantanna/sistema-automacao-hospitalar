// Karma configuration
// Generated on Thu Mar 23 2017 11:39:42 GMT-0300 (E. South America Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'assets/libs/js/moment-with-locales.min.js',
        'assets/libs/js/jquery.min.js',
        'assets/libs/js/angular.min.js',
        'assets/libs/js/angular-resource.js',
        'assets/libs/js/angular-messages.min.js',
        'assets/libs/js/angular-sanitize.min.js',
        'assets/libs/js/angular-locale_pt-br.js',
        'assets/libs/js/angular-translate.min.js',
        'assets/libs/js/angular-translate-loader-static-files.min.js',
        'assets/libs/js/angular-input-masks-dependencies.min.js',
        'assets/libs/js/angular-input-masks.br.min.js',
        'assets/libs/js/angular-ui-router.min.js',
        'assets/libs/js/angular-growl.min.js',
        'assets/libs/js/smart-table.min.js',
        'assets/libs/js/angular-animate.min.js',
        'assets/libs/js/ui-bootstrap-tpls.min.js',
        'assets/libs/js/angular-breadcrumb.min.js',
        'assets/libs/js/ng-file-upload-shim.min.js',
        'assets/libs/js/ng-file-upload.min.js',
        'assets/libs/js/datetime-picker.min.js',
        'assets/libs/js/bootstrap-toggle.min.js',
        'assets/libs/js/checklist-model.js',
        'assets/libs/js/textAngular-rangy.min.js',
        'assets/libs/js/textAngular-sanitize.min.js',
        'assets/libs/js/textAngular.min.js',
        'assets/libs/js/angular-br-filters.min.js',
        'assets/libs/js/angular.dcb-img-fallback.min.js',
        'assets/libs/js/jquery.ez-plus.js',
        'assets/libs/js/angular-ezplus.js',
        'assets/libs/js/ng-magnify.js',
        'assets/libs/js/image-zoom.min.js',
        'app/directives/validation/showErrorsValidation.js',
        'app/directives/bootstrap-toggle/bootstrap-toggle-angular.js',
        'app/directives/table/trigger-table-reload.js',
        'app/directives/table/stSelectAll.js',
        'app/directives/filter/filter-button.js',
        'app/directives/img-fallback/img-fallback.js',
        'app/app.js',
        'app/boleto/boleto.details.js',
        'app/shared/page/header.js',
        'app/shared/page/menu-right.js',
        'app/home/materiais-medicamentos-hospitalares.js',
        'app/usuario/usuario.js',
        'app/usuario/usuarioIndex.js',
        'app/fornecedor/fornecedor.js',
        'app/fornecedor/fornecedorIndex.js',
        'app/contrato/contrato.js',
        'app/contrato/contratoIndex.js',
        'app/contrato/contratoLoad.js',
        'app/sequencia/sequencia.js',
        'app/sequencia/sequenciaIndex.js',
        'app/boleto/boleto.js',
        'app/boleto/boleto.index.js',
        'app/boleto/boleto.tratamento.js',
        'app/boleto/boleto.tratamento.js',
        'app/boleto/boletoBySequencia.index.js',
        'app/pagina/pagina.js',
        'app/pagina/pagina.index.js',
        'app/pagina/pagina.validacao.js',
        'app/email/email.js',
        'app/email/emailIndex.js',
        'app/configuracao/configuracao.info.js',
        'app/configuracao/configuracao.index.js',
        'app/configuracaoCobranca/configuracaoCobranca.info.js',
        'app/configuracaoCobranca/configuracaoCobranca.index.js',
        'app/condicaoPagamento/condicaoPagamento.js',
        'app/condicaoPagamento/condicaoPagamento.index.js',
        'app/callCenter/callCenter.js',
        'app/callCenter/callCenter.index.js',
        'app/pagamento/pagamentoIndex.js',
        'app/pagamento/pagamentoLoad.js',
        'app/callCenter/teste.js',
        'app/pagina/pagina.details.js',
        'app/callCenter/historicoCobranca.index.js',
        'app/callCenter/historicoCobranca.js',
        'app/mapeamento/mapeamento.index.js',
        'app/mapeamento/mapeamento.js',
        'app/sequencia/sequenciaVisaoGeral.js',

        'node_modules/angular-mocks/angular-mocks.js',

        'spec/**/*Spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
};
