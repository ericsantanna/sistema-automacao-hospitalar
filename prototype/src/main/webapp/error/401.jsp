<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>WFP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="stylesheet" href="../assets/libs/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/libs/css/font-awesome.min.css">
    <%--<link rel="stylesheet" href="../assets/libs/css/angular-growl.min.css">--%>
    <%--<link rel="stylesheet" href="../assets/libs/css/awesome-bootstrap-checkbox.css">--%>
    <%--<link rel="stylesheet" href="../assets/libs/css/bootstrap-toggle.min.css">--%>
    <%--<link rel="stylesheet" href="../assets/libs/css/textAngular.css">--%>
    <link rel="stylesheet" href="../assets/css/styles.css">

    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/moment.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/moment.ptBR.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/jquery.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-resource.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-messages.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-sanitize.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-locale_pt-br.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-translate.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-translate-loader-static-files.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-input-masks-dependencies.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-input-masks.br.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-ui-router.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-growl.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/smart-table.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-animate.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/ui-bootstrap-tpls.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-breadcrumb.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/ng-file-upload-shim.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/ng-file-upload.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/datetime-picker.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/bootstrap-toggle.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/checklist-model.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/textAngular-rangy.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/textAngular-sanitize.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/textAngular.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/pdf.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/pdf.worker.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/angular-pdf.min.js"></script>--%>
    <%--<script type="text/javascript" charset="UTF-8" src="../assets/libs/js/collapse.js"></script>--%>
</head>
<body>
<div class="container fluid">
    <div class="alert alert-error text-center" style="margin-top: 25%; font-size: large">
        <strong>Você não está logado na aplicação ou não tem permissão para acessar esse recurso</strong>
    </div>
</div>
</body>
</html>