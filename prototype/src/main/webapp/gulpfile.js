var build = 'debug';

var gulp = require('gulp'), // Gulp!
	runSequence = require('run-sequence'),
	debug = require('gulp-debug'), // Logging no console dos arquivos no stream
	gulpif = require('gulp-if'),
	browserSync = require('browser-sync').create(),
	del = require('del'),
	flatten = require('gulp-flatten'), // Coloca todos os arquivos em uma stream no mesmo nivel (sem informação de diretórios)
    sass = require('gulp-ruby-sass'), // Processador SASS
    sourcemaps = require('gulp-sourcemaps'),
    notify = require("gulp-notify"), // Dependência de outros plugins
    bower = require('gulp-bower'), // Processa bower.json
    cssnano = require('gulp-cssnano'), // Minificação CSS
    // minifyHtml = require('gulp-minify-html'), // Minificação HTML
    concat = require('gulp-concat'), // Concatenação de arquivos
    uglify = require('gulp-uglify'), // Minificação JS
    gulpFilter = require('gulp-filter'), // Filtra o stream
    mainBowerFiles = require('main-bower-files'), // Pega somente os arquivos necessários de cada lib bower
    wait = require('gulp-wait'), // gzip nos fontes
    gzip = require('gulp-gzip'), // gzip nos fontes
    CacheBuster = require('gulp-cachebust'), // Adiciona um hash em arquivos que queremos resetar do cache do cliente, alterando suas referências no html
    cachebust = new CacheBuster();

var libs = require('./libs.json'); // Lista de arquivos principais a serem pegos pelo plugin gulp-main-bower-files

var paths = {
	debug: {
		app: './app',
		shared: './app/shared',
		css: './assets/css',
		js: './assets/js',
		images: './assets/images',
		fonts: './assets/fonts',
		libs: './assets/libs',
		sass: './sass',
		bower: './bower_components'
	},
	release: {
		app: './release/app',
		shared: './release/shared',
		css: './release/assets/css',
		js: './release/assets/js',
		images: './release/assets/images',
		fonts: './release/assets/fonts',
		libs: './release/assets/libs'
	}
};

var browserSyncConfig = {
	proxy: '127.0.0.1:9090',
	startPath: '/wfp/app',
	browser: "google chrome"
};

gulp.task('build-sass', function() {
    return sass(paths.debug.sass + '/*.sass', {sourcemap: true})
	    .on('error', function (err) {
	        console.error('Error!', err.message);
	    })
	    .pipe(sourcemaps.write('./', {
	        includeContent: false,
	        sourceRoot: paths.debug.sass
	    }))
        .pipe(gulp.dest(paths.debug.css))
        .pipe(browserSync.stream({match: '**/*.css'}));
});

//Rerun the task when a file changes
gulp.task('watch-sass', function() {
	gulp.watch(paths.debug.sass + '/**/*.sass', ['build-sass']);
});

gulp.task('browser-sync', function() {
    browserSync.init(browserSyncConfig);
//    gulp.watch(paths.debug.sass + '/**/*.sass', ['build-sass'])
//    gulp.watch([
//        paths.debug.app + '/**/*.*',
//        paths.debug.app + '/**/*.js'
//        './assets/**/*.*'
//        paths.debug.app + '/**/*.html',
//        paths.debug.app + '/**/*.js',
//        paths.debug.app + '/**/*.css',
//        paths.debug.css + '/**/*.html',
//        paths.debug.js + '/**/*.js',
//        paths.debug.shared + '/**/*.css'
//        paths.debug.app + '/**/*.jsp',
//        paths.debug.app + '/**/*.html',
//    	paths.debug.app + '/**/*.js',
//    	paths.debug.app + '/**/*.css',
//    	paths.debug.js + '/**/*.js',
//    	paths.debug.libs + '/**/*.js',
//    	paths.debug.libs + '/**/*.css'
//	])
//	.on('change', browserSync.reload);
//    gulp.watch(deployment + '/app').on('change', browserSync.reload);gulp.watch("app/scss/*.scss", ['sass']);
//    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('cleanup', function() {
	return del([
	            './release',
	            paths[build].libs + '/js/vendor.min.js',
	            paths[build].libs + '/css/vendor.min.css',
	            paths[build].js + '/scripts.min.js',
	            paths[build].css + '/styles.min.css'
	       ]);
});

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(paths.debug.bower));
});

gulp.task('build-libs', function() {
    var filterJs = gulpFilter(['**/*.js', '**/*.json', '**/*.swf'], { restore: true });
    var filterCss = gulpFilter('**/*.css', { restore: true });
    var filterFonts = gulpFilter(['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff', '**/*.woff2'], { restore: true });
    var filterImages = gulpFilter(['**/*.jpg', '**/*.jpeg', '**/*.png', '**/*.gif'], { restore: true });
//    var filterSass = gulpFilter(['**/*.sass', '**/*.scss'], { restore: true });

    /*
     * Lista os arquivos listados como "main" dos bower.json de cada modulo 
     */
//    return gulp.src('./bower.json')
//	    
//    	.pipe(mainBowerFiles({
//        	overrides: libs // Sobrescrevo alguns desses "main", o código está em libs.json
//        }))
    return gulp.src(mainBowerFiles())
        .pipe(debug({title: 'lib:'})) // log
        
        // Filtro somente javascripts
        .pipe(filterJs)
        .pipe(gulpif(build === 'release', concat('vendor.min.js'))) // Concateno todos os js de todas as libs em 1 arquivo
        .pipe(gulpif(build === 'release', uglify())) // Minificação
        .pipe(gulpif(build === 'release', cachebust.resources())) // Adiciono um hash no nome do arquivo para forçar a atualização de cache no cliente
//        .pipe(gzip())
        .pipe(gulp.dest(paths[build].libs + '/js'))
        .pipe(filterJs.restore)
        
        // Filtro somente css
        .pipe(filterCss)
        .pipe(gulpif(build === 'release', concat('vendor.min.css'))) // Concateno todos os css de todas as libs em 1 arquivo
        .pipe(gulpif(build === 'release', cssnano())) // Minificação
        .pipe(gulpif(build === 'release', cachebust.resources())) // Adiciono um hash no nome do arquivo para forçar a atualização de cache no cliente
//        .pipe(gzip())
        .pipe(gulp.dest(paths[build].libs + '/css'))
        .pipe(filterCss.restore)
        
        // Filtro somente fontes
        .pipe(filterFonts)
        .pipe(flatten()) // Remove definições de estrutura de diretórios, todos vão pro mesmo nível
        .pipe(gulp.dest(paths[build].libs + '/fonts'))
        .pipe(filterFonts.restore)
        
        // Filtro somente fontes
        .pipe(filterImages)
//        .pipe(flatten()) // Remove definições de estrutura de diretórios, todos vão pro mesmo nível
        .pipe(gulp.dest(paths[build].libs + '/css/img'))
        .pipe(filterImages.restore);
});

gulp.task('build-js', function() {
	return gulp.src(paths.debug.js + '/*.js')
	.pipe(gulpif(build === 'release', concat('scripts.min.js'))) // Concateno todos os js em 1 arquivo
	.pipe(uglify()) // Minificação
	.pipe(gulpif(build === 'release', cachebust.resources())) // Adiciono um hash no nome do arquivo para forçar a atualização de cache no cliente
//	.pipe(gzip())
	.pipe(gulp.dest(paths[build].js));
});

gulp.task('build-css', function() {
	return gulp.src(paths.debug.css + '/*.css')
	.pipe(gulpif(build === 'release', concat('styles.min.css'))) // Concateno todos os js em 1 arquivo
	.pipe(cssnano()) // Minificação
	.pipe(gulpif(build === 'release', cachebust.resources())) // Adiciono um hash no nome do arquivo para forçar a atualização de cache no cliente
//	.pipe(gzip())
	.pipe(gulp.dest(paths[build].css));
});

gulp.task('build-shared', function() {
	return gulp.src([
         paths.debug.shared + '/**/*.jsp',
         paths.debug.shared + '/**/*.html'
     ])
         
     .pipe(gulpif(build === 'release', cachebust.references()))
     .pipe(gulp.dest(paths[build].shared));
});

gulp.task('build-app', function() {
    var filterJs = gulpFilter('**/*.js', { restore: true });
    var filterHtml = gulpFilter(['**/*.jsp', '**/*.html'], { restore: true });

	return gulp.src([
         './index.jsp',
         paths.debug.app + '/**/*.jsp',
         paths.debug.app + '/**/*.html',
         paths.debug.app + '/**/*.js'
     ])
     
     // HTML
     .pipe(filterHtml)
     .pipe(gulpif(build === 'release', cachebust.references()))
//	.pipe(gulpif(build === 'release', minifyHtml({ empty: true })))
//	.pipe(gzip())
     .pipe(gulp.dest(paths[build].app))
     .pipe(filterHtml.restore)
     
     // JS
     .pipe(filterJs)
     .pipe(gulpif(build === 'release', uglify()))
//     .pipe(gzip())
     .pipe(gulp.dest(paths[build].app))
     .pipe(filterJs.restore);
});

//gulp.task('debug', ['watch-sass', 'browser-sync']);
gulp.task('debug', function() {
	runSequence('cleanup', 'bower', 'build-sass', ['build-libs', 'build-js', 'build-css'], ['build-app', 'build-shared'], 'watch-sass');
});

gulp.task('release', function() {
	build = 'release';
	runSequence('cleanup', 'bower', 'build-sass', ['build-libs', 'build-js', 'build-css'], ['build-app', 'build-shared']);
});