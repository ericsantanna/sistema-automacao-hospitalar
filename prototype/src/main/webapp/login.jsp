<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="WfpApp">
<head>
	<title>WFP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="stylesheet" href="assets/libs/css/bootstrap.min.css">h
	<link rel="stylesheet" href="assets/libs/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/libs/css/angular-growl.min.css">
	<%--<link rel="stylesheet" href="assets/css/global.css">--%>
    <link rel="stylesheet" href="assets/css/not-ie.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="login.css">

	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/jquery.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/angular.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/angular-animate.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/ui-bootstrap-tpls.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/angular-growl.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/angular-messages.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="assets/libs/js/angular-sanitize.min.js"></script>
	<script type="text/javascript" charset="UTF-8" src="login.js"></script>

    <script type="text/javascript" charset="UTF-8" src="assets/js/background-content.js"></script>
</head>
<body class="vertical-center" ng-controller="LoginController">
    <span class="background-content"></span>
    <div growl></div>
	<nav id="header" class="navbar navbar-default navbar-fixed-top">
		<div class="row">
			<div class="col-xs-6 col-sm-8">
				<h1 style="margin-left: 40px;">WFP
					<small class="hidden-xs" style="margin-left: 15px;">Workflow de pagamentos</small></h1>
			</div>
			<div id="logo-acn" class="pull-right">
			</div>
		</div>
	</nav>
	<input id="authError" type="hidden" value="${param.authError}">
	<div class="container">
		<div id="messages" class="hidden alert alert-danger alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">Close</span>
			</button>
			<div class="validation-message"></div>
		</div>
		<div id="login-panel" class="col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Acesso ao WFP</h3>
				</div>
				<div class="panel-body">
					<ng-form name="formLogin">
	                    <div class="row">
                            <div class="form-group col-xs-12" show-errors>
                                <div class="input-group">
	                                <span class="input-group-addon">
	                                	<img src="assets/images/login/glyphicons-4-user2.png" width="20px" data-toggle="tooltip" data-placement="top" title="Usuário">
	                                </span>
                                    <input type="text" name="login" class="form-control" placeholder="Usuário" ng-model="usuario.login" ng-disabled="modo === 'changePassword'" required ng-minlength="3" ng-maxlength="255">
                                </div>
                                <div ng-messages="formLogin.login.$error" class="error-msg text-danger">
									<div ng-message="required" class="message">Campo obrigatório</div>
									<div ng-message="minlength" class="message">Deve ter ao menos 3 caracteres</div>
									<div ng-message="maxlength" class="message">Deve ter ao menos 255 caracteres</div>
                                </div>
                            </div>
	                    </div>
	                    <div class="row" ng-if="modo === 'login'">
                            <div class="form-group col-xs-12" show-errors>
                                <div class="input-group">
	                                <span class="input-group-addon">
	                                	<img src="assets/images/login/glyphicons-45-keys2.png" width="20px" data-toggle="tooltip" data-placement="top" title="Senha">
	                                </span>
                                    <input type="password" name="senha" class="form-control" placeholder="Senha" ng-model="usuario.senha" required ng-minlength="6" ng-maxlength="255">
                                </div>
                                <div ng-messages="formLogin.senha.$error" class="error-msg text-danger">
									<div ng-message="required" class="message">Campo obrigatório</div>
									<div ng-message="minlength" class="message">Deve ter ao menos 6 caracteres</div>
									<div ng-message="maxlength" class="message">Deve ter ao menos 255 caracteres</div>
                                </div>
                            </div>
	                    </div>
                        <div class="row" ng-if="modo === 'changePassword'">
                            <div class="col-xs-12">
                                <strong class="text-danger">Altere sua senha:</strong>
                            </div>
                        </div>
                        <div class="row" ng-if="modo === 'changePassword'">
                            <div class="form-group col-xs-12" show-errors>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <img src="assets/images/login/glyphicons-45-keys2.png" width="20px" data-toggle="tooltip" data-placement="top" title="Senha">
                                    </span>
                                    <input type="password" name="senha" class="form-control" placeholder="Senha" ng-change="evaluatePassword(usuario.senha)" ng-model="usuario.senha" required ng-minlength="6" ng-maxlength="255">
                                </div>
                                <div ng-messages="formLogin.senha.$error" class="error-msg text-danger">
                                    <div ng-message="required" class="message">Campo obrigatório</div>
                                    <div ng-message="minlength" class="message">Deve ter ao menos 6 caracteres</div>
                                    <div ng-message="maxlength" class="message">Deve ter ao menos 255 caracteres</div>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="modo === 'changePassword'">
                            <div class="form-group col-xs-12" show-errors>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <img src="assets/images/login/glyphicons-45-keys2.png" width="20px" data-toggle="tooltip" data-placement="top" title="Senha">
                                    </span>
                                    <input type="password" name="confirmacao" class="form-control" placeholder="Confirmação" ng-model="confirmacao" required ng-minlength="6" ng-maxlength="255">
                                </div>
                                <div ng-messages="formLogin.confirmacaoSenha.$error" class="error-msg text-danger">
                                    <div ng-message="required" class="message">Campo obrigatório</div>
                                    <div ng-message="minlength" class="message">Deve ter ao menos 6 caracteres</div>
                                    <div ng-message="maxlength" class="message">Deve ter ao menos 255 caracteres</div>
                                </div>
                            </div>
                        </div>
                        <div class="row" ng-if="modo === 'changePassword'">
                            <div class="col-xs-12">
                                <%--<div class="alert" ng-class="{'alert-info': !strength || strength == 0, 'alert-danger': strength < 230, 'alert-warning': strength >= 230 && strength < 300, 'alert-success': strength >= 300}">--%>
                                <div>
                                    <strong>
                                        Fator de complexidade:
                                        <span class="fa pull-right" ng-class="{'fa-hand-rock-o text-warning': !strength, 'fa-thumbs-o-down text-danger': strength < 230, 'fa-thumbs-o-up text-info': strength >= 230 && strength < 300, 'fa-hand-spock-o text-success': strength >= 300}"></span>
                                    </strong>
                                    <uib-progressbar value="strength" min="0" max="300" type="{{complexityAlert(strength)}}">{{strength | number:0}}</uib-progressbar>
                                </div>
                            </div>
                        </div>
						<a href="#" ng-click="esqueciASenha()" ng-hide="modo === 'changePassword'">Esqueci a senha...</a>
	                    <div class="row">
                            <div class="col-md-12" ng-show="modo === 'login'">
                                <button class="btn btn-lg btn-primary btn-block" ng-click="entrar(usuario)">
                                    <span class="glyphicon glyphicon-log-in"></span>  Entrar
                                </button>
                            </div>
                            <div class="col-md-5" ng-show="modo === 'forgotPassword'">
                                <button class="btn btn-lg btn-default btn-block" ng-click="cancelar()">
		                            <span class="glyphicon glyphicon-log-in"></span>  Cancelar
		                        </button>
                            </div>
                            <div class="col-md-7" ng-show="modo === 'changePassword'">
                                <button class="btn btn-lg btn-primary btn-block" ng-click="alterarSenha(usuario, confirmacao)">
                                    <span class="glyphicon glyphicon-log-in"></span>  Alterar senha
                                </button>
	                        </div>
							<div class="col-md-7" ng-show="modo === 'forgotPassword'">
								<button class="btn btn-lg btn-primary btn-block" ng-click="enviarNovaSenha(usuario)">
									<span class="glyphicon glyphicon-log-in"></span>  Enviar nova senha
								</button>
							</div>
						</div>
					</ng-form>
				</div>
			</div>
		</div>
	</div>
	<div class="navbar navbar-fixed-bottom hidden-xs hidden-sm">
	   <div class="container">
			<div class="row" style="padding: 20px;">
				<div class="col-md-4 bordaDireita">
					<h4>Solução</h4>
					<p><b>Gerenciamento do fluxo de trabalho do processo de novos contratos, alterações e cadastro dos Contratos de Locação do Cliente Oi.</b></p>
				</div>
				<div class="col-md-4 clearfix bordaDireita" style="padding-left: 1.5em;">
					<h4>Mobilidade</h4>
					<div id="qr-code" class="pull-right"></div>
					<p><b>Você também pode acessar a ferramenta através do seu dispositivo móvel.</b></p>
				</div>     
				<div class="col-md-4">
					<div id="accenture-brand" class="pull-right"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>