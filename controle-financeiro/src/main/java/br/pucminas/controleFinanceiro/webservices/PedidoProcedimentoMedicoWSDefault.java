package br.pucminas.controleFinanceiro.webservices;

import br.pucminas.controleFinanceiro.accessLogger.AccessLoggerInterceptor;
import br.pucminas.controleFinanceiro.pedidoProcedimentoMedico.PedidoProcedimentoMedico;
import br.pucminas.controleFinanceiro.pedidoProcedimentoMedico.PedidoProcedimentoMedicoDAO;
import br.pucminas.controleFinanceiro.pedidoProcedimentoMedico.PedidoProcedimentoMedicoWS;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.controleFinanceiro.pedidoProcedimentoMedico.PedidoProcedimentoMedicoWS")
public class PedidoProcedimentoMedicoWSDefault implements PedidoProcedimentoMedicoWS {

    @Inject private PedidoProcedimentoMedicoDAO dao;

    @Override
    public PedidoProcedimentoMedico get(long id) {
        return dao.get(id);
    }

    @Override
    public List<PedidoProcedimentoMedico> list() {
        return dao.list();
    }
}
