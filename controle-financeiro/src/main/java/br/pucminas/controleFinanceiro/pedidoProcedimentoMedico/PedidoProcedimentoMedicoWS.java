package br.pucminas.controleFinanceiro.pedidoProcedimentoMedico;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface PedidoProcedimentoMedicoWS {
    PedidoProcedimentoMedico get(long id);
    List<PedidoProcedimentoMedico> list();
}
