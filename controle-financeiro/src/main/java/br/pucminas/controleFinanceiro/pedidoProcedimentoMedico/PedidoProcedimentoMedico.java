package br.pucminas.controleFinanceiro.pedidoProcedimentoMedico;

import br.pucminas.controleFinanceiro.paciente.Paciente;
import br.pucminas.controleFinanceiro.planoSaude.PlanoSaude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Eric Sant'Anna
 */
@Entity
@XmlRootElement
@Data @NoArgsConstructor
public class PedidoProcedimentoMedico implements Serializable {
    private static final long serialVersionUID = 6663370011059908286L;

    public enum Status {PENDENTE, APROVADO, NAO_APROVADO}

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private BigDecimal valor;

    @ManyToOne
    private Paciente paciente;

    @ManyToOne
    private PlanoSaude planoSaude;

    @Enumerated(EnumType.STRING)
    private Status status = Status.PENDENTE;
}
