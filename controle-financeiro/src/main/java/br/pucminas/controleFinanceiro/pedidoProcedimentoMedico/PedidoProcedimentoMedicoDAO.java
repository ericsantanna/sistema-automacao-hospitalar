package br.pucminas.controleFinanceiro.pedidoProcedimentoMedico;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class PedidoProcedimentoMedicoDAO {
    @PersistenceContext
    private EntityManager em;

    public PedidoProcedimentoMedico get(long id) {
        return em.find(PedidoProcedimentoMedico.class, id);
    }

    public List<PedidoProcedimentoMedico> list() {
        return em.createQuery("select p from PedidoProcedimentoMedico p", PedidoProcedimentoMedico.class).getResultList();
    }
}
