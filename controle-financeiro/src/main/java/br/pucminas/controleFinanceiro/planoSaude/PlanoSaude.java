package br.pucminas.controleFinanceiro.planoSaude;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@Entity
@XmlRootElement
@Data @NoArgsConstructor
public class PlanoSaude implements Serializable {
    private static final long serialVersionUID = -4095956663408548694L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;

    public PlanoSaude(String nome) {
        this.nome = nome;
    }
}
