package br.pucminas.controleMateriaisMedicamentos.webservices;

import br.pucminas.controleMateriaisMedicamentos.accessLogger.AccessLoggerInterceptor;
import br.pucminas.controleMateriaisMedicamentos.medicamentos.Medicamento;
import br.pucminas.controleMateriaisMedicamentos.medicamentos.MedicamentoDAO;
import br.pucminas.controleMateriaisMedicamentos.medicamentos.MedicamentoWS;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.controleMateriaisMedicamentos.medicamentos.MedicamentoWS")
public class MedicamentoWSDefault implements MedicamentoWS {

    @Inject private MedicamentoDAO dao;

    @Override
    public Medicamento get(long id) {
        return dao.get(id);
    }

    @Override
    public List<Medicamento> list() {
        return dao.list();
    }
}
