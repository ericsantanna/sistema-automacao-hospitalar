package br.pucminas.controleMateriaisMedicamentos.webservices;

import br.pucminas.controleMateriaisMedicamentos.accessLogger.AccessLoggerInterceptor;
import br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares.MaterialHospitalar;
import br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares.MaterialHospitalarDAO;
import br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares.MaterialHospitalarWS;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Interceptors({AccessLoggerInterceptor.class})
@WebService(endpointInterface = "br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares.MaterialHospitalarWS")
public class MaterialHospitalarWSDefault implements MaterialHospitalarWS {

    @Inject private MaterialHospitalarDAO dao;

    @Override
    public MaterialHospitalar get(long id) {
        return dao.get(id);
    }

    @Override
    public List<MaterialHospitalar> list() {
        return dao.list();
    }
}
