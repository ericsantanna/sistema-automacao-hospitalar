package br.pucminas.controleMateriaisMedicamentos.medicamentos;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class MedicamentoDAO {
    @PersistenceContext
    private EntityManager em;

    public Medicamento get(long id) {
        return em.find(Medicamento.class, id);
    }

    public List<Medicamento> list() {
        return em.createQuery("select m from Medicamento m", Medicamento.class).getResultList();
    }
}
