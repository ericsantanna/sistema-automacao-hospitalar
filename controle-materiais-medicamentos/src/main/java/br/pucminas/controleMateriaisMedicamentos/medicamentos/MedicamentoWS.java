package br.pucminas.controleMateriaisMedicamentos.medicamentos;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface MedicamentoWS {
    Medicamento get(long id);
    List<Medicamento> list();
}
