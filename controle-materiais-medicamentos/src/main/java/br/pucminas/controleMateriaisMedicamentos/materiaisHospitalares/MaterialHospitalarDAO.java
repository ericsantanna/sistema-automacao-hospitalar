package br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@Transactional
@RequestScoped
public class MaterialHospitalarDAO {
    @PersistenceContext
    private EntityManager em;

    public MaterialHospitalar get(long id) {
        return em.find(MaterialHospitalar.class, id);
    }

    public List<MaterialHospitalar> list() {
        return em.createQuery("select m from MaterialHospitalar m", MaterialHospitalar.class).getResultList();
    }
}
