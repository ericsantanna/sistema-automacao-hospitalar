package br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Eric Sant'Anna
 */
@Entity
@XmlRootElement
@Data @NoArgsConstructor
public class MaterialHospitalar implements Serializable {
    private static final long serialVersionUID = -7345343126973697832L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;
    private String marca;
    private int quantidade;
}
