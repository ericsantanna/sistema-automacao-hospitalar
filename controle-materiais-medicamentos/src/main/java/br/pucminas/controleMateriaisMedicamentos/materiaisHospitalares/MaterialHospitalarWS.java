package br.pucminas.controleMateriaisMedicamentos.materiaisHospitalares;

import javax.jws.WebService;
import java.util.List;

/**
 * @author Eric Sant'Anna
 */
@WebService
public interface MaterialHospitalarWS {
    MaterialHospitalar get(long id);
    List<MaterialHospitalar> list();
}
