DELETE FROM sensor;

INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'General Eletric Co.', 'GEMonitor', 'a4f08xw8j0ji226kvbd39', 'GEMonitor', '2.1c', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'General Eletric Co.', 'GEMonitor', '7h72bknx86asikj289hxh', 'GEMonitor', '2.1c', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'General Eletric Co.', 'GEMonitor', 'v5xvhh38sknlauiozmls8', 'GEMonitor', '2.1c', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'Medtronic Inc.', 'MedtrHg 2017', '1j1b6c00fj64cz7gjr679ks', 'Medtronic Revo2', '2.1.25674', 'STANDBY');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'Medtronic Inc.', 'MedtrHg 2017', '8h8ggxzvnfxd24l0xvradd2', 'Medtronic Revo2', '2.1.25674', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'Medtronic Inc.', 'MedtrHg 2017', '8sjsbyvghjs74hs33zvbb95', 'Medtronic Revo2', '2.1.25674', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'Siemens AG', 'Siemens BPM 6', '9sjn476bsjkn', 'SGesundheitsmonitor', '16.0.1.2334', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'Siemens AG', 'Siemens BPM 6', '3bhx986glkmm', 'SGesundheitsmonitor', '16.0.1.2334', 'OFFLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('PRESSAO_ARTERIAL', 'Siemens AG', 'Siemens BPM 6', 'bcv23ysi975h', 'SGesundheitsmonitor', '16.0.1.2334', 'ONLINE');

INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'General Eletric Co.', 'GEMonitor', 'a12avgbdju7865ghdb9bb', 'GEMonitor', '2.1c', 'OFFLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'General Eletric Co.', 'GEMonitor', '8nd6bz5gbdik311nv8g9g', 'GEMonitor', '2.1c', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'General Eletric Co.', 'GEMonitor', '0zzdfb52njb36vc07b54h', 'GEMonitor', '2.1c', 'OFFLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'Medtronic Inc.', 'MedtrHg 2017', 'p0iv53cx6h8md7vbs52xvgk', 'Medtronic Revo2', '2.1.25674', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'Medtronic Inc.', 'MedtrHg 2017', '4f6g9jhc1npopjmb239n0n1', 'Medtronic Revo2', '2.1.25674', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'Medtronic Inc.', 'MedtrHg 2017', '3hz8nl334m06bsaq1qe328j', 'Medtronic Revo2', '2.1.25674', 'STANDBY');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'Siemens AG', 'Siemens BPM 6', 'z9jb76bib5fs', 'SGesundheitsmonitor', '16.0.1.2334', 'ONLINE');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'Siemens AG', 'Siemens BPM 6', '7ybbgha5fal2', 'SGesundheitsmonitor', '16.0.1.2334', 'STANDBY');
INSERT INTO sensor(tipo, marca, modelo, serialnumber, software, versao, status)
VALUES ('BATIMENTO_CARDIACO', 'Siemens AG', 'Siemens BPM 6', 'njnv2c3la97y', 'SGesundheitsmonitor', '16.0.1.2334', 'STANDBY');