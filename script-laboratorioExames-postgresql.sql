DELETE FROM hemograma;
DELETE FROM paciente;

INSERT INTO paciente(id, nome) VALUES (1, 'Ana Almeida');
INSERT INTO paciente(id, nome) VALUES (2, 'Bia Barbosa');
INSERT INTO paciente(id, nome) VALUES (3, 'Carlos Cardoso');
INSERT INTO paciente(id, nome) VALUES (4, 'Diego Dias');
INSERT INTO paciente(id, nome) VALUES (5, 'Edson Esteves');
INSERT INTO paciente(id, nome) VALUES (6, 'Fernanda Farias');
INSERT INTO paciente(id, nome) VALUES (7, 'Glória Galvão');

INSERT INTO public.hemograma(
  id, dataPedido, dataEntrega, status, eritocitos, chcm, hcm, rdw, vcm, hematocritos, hemoglobinas,
  basofilos, bastonetes, eosinofilos, linfocitosatipicos, linfocitostipicos,
  monocitos, metamielocitos, mielocitos, segmentados, neutrofilos,
  outros, leucocitos, plaquetas, paciente_id)
VALUES (1, '2017-06-01 09:30:45', '2017-06-01 11:20:16', 'PRONTO', 5.66, 33.3, 29.0, 13.0, 86.9, 49.2, 16.4, 0.4, 0.0, 3.9, 0.0, 36.5,  5.1, 0.0, 0.0, 54.1, 54.1, 0.0, 9210.0, 328000.0, 1);

INSERT INTO public.hemograma(id, dataPedido, dataEntrega, status, paciente_id)
VALUES (2, '2017-06-01 15:45:01', '2017-06-01 16:50:00', 'NAO_REALIZADO', 2);

INSERT INTO public.hemograma(
  id, dataPedido, dataEntrega, status, eritocitos, chcm, hcm, rdw, vcm, hematocritos, hemoglobinas,
  basofilos, bastonetes, eosinofilos, linfocitosatipicos, linfocitostipicos,
  monocitos, metamielocitos, mielocitos, segmentados, neutrofilos,
  outros, leucocitos, plaquetas, paciente_id)
VALUES (3, '2017-06-02 21:09:11', '2017-06-02 21:31:50', 'PRONTO', 5.66, 33.3, 29.0, 13.0, 86.9, 49.2, 16.4, 0.4, 0.0, 3.9, 0.0, 36.5,  5.1, 0.0, 0.0, 54.1, 54.1, 0.0, 9210.0, 328000.0, 2);

INSERT INTO public.hemograma(
  id, dataPedido, dataEntrega, status, eritocitos, chcm, hcm, rdw, vcm, hematocritos, hemoglobinas,
  basofilos, bastonetes, eosinofilos, linfocitosatipicos, linfocitostipicos,
  monocitos, metamielocitos, mielocitos, segmentados, neutrofilos,
  outros, leucocitos, plaquetas, paciente_id)
VALUES (4, '2017-06-03 12:12:18', '2017-06-03 12:52:13', 'PRONTO', 5.66, 33.3, 29.0, 13.0, 86.9, 49.2, 16.4, 0.4, 0.0, 3.9, 0.0, 36.5,  5.1, 0.0, 0.0, 54.1, 54.1, 0.0, 9210.0, 328000.0, 3);

INSERT INTO public.hemograma(
  id, dataPedido, dataEntrega, status, eritocitos, chcm, hcm, rdw, vcm, hematocritos, hemoglobinas,
  basofilos, bastonetes, eosinofilos, linfocitosatipicos, linfocitostipicos,
  monocitos, metamielocitos, mielocitos, segmentados, neutrofilos,
  outros, leucocitos, plaquetas, paciente_id)
VALUES (5, '2017-06-04 10:05:56', '2017-06-04 16:22:49', 'PRONTO', 5.66, 33.3, 29.0, 13.0, 86.9, 49.2, 16.4, 0.4, 0.0, 3.9, 0.0, 36.5,  5.1, 0.0, 0.0, 54.1, 54.1, 0.0, 9210.0, 328000.0, 4);

INSERT INTO public.hemograma(id, dataPedido, status, paciente_id)
VALUES (6, '2017-06-05 11:20:23', 'PENDENTE', 5);

INSERT INTO public.hemograma(id, dataPedido, status, paciente_id)
VALUES (7, '2017-06-05 18:03:40', 'PENDENTE', 6);

INSERT INTO public.hemograma(id, dataPedido, dataEntrega, status, paciente_id)
VALUES (8, '2017-06-06 15:40:34', '2017-06-06 19:39:14', 'NAO_REALIZADO', 7);

INSERT INTO public.hemograma(id, dataPedido, status, paciente_id)
VALUES (9, '2017-06-07 17:21:02', 'PENDENTE', 7);
